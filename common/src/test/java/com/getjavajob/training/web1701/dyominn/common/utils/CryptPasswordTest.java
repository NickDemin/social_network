package com.getjavajob.training.web1701.dyominn.common.utils;

import org.junit.Test;

import static com.getjavajob.training.web1701.dyominn.common.utils.CryptPassword.check;
import static com.getjavajob.training.web1701.dyominn.common.utils.CryptPassword.hash;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertTrue;

public class CryptPasswordTest {

    @Test
    public void hashAndCheck() {
        String password = "UserPassword123";
        String hash = hash(password);
        assertNotEquals(password, hash);
        assertTrue(check(password, hash));
    }
}