package com.getjavajob.training.web1701.dyominn.common.utils;

import org.mindrot.jbcrypt.BCrypt;

public class CryptPassword {
    private static final int WORKLOAD = 10;

    public static String hash(String password) {
        String salt = BCrypt.gensalt(WORKLOAD);
        return BCrypt.hashpw(password, salt);
    }

    public static boolean check(String password, String storedHash) {
        if (null == storedHash || !storedHash.startsWith("$2a$")) {
            throw new IllegalArgumentException("Invalid hash provided for comparison");
        }
        return BCrypt.checkpw(password, storedHash);
    }
}