package com.getjavajob.training.web1701.dyominn.common;

public interface Entity {

    long getId();

    void setId(long id);
}