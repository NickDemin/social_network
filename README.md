# Yet Another Social Network

** Functionality: **

+ authentication  
+ registration profile/group  
+ display profile/group  
+ edit profile/group  
+ upload avatar  
+ friendship/membership relations  
+ ajax autocomplete search  
+ ajax search with pagination  
+ update profile from xml file  
+ export profile data to xml file  

** Tools: **  
JDK 8, Spring 4 (MVC), JPA 2 / Hibernate 5, XStream, jQuery 3, Bootstrap 3, JUnit 4, Mockito 2, Maven 3, Git / Bitbucket, Tomcat 8, MySQL, IntelliJIDEA 2017.  


** Notes: **  
SQL ddl is located in the `dao/src/main/resources/initDB_MySQL.sql`

_  
**Dyomin Nikolay**  
Тренинг getJavaJob  
[http://www.getjavajob.com](http://www.getjavajob.com)  