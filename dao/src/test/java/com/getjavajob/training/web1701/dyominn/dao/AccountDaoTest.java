package com.getjavajob.training.web1701.dyominn.dao;

import com.getjavajob.training.web1701.dyominn.common.models.Account;
import com.getjavajob.training.web1701.dyominn.common.models.Group;
import com.getjavajob.training.web1701.dyominn.common.models.Phone;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.PersistenceException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import static com.getjavajob.training.web1701.dyominn.common.models.PhoneType.HOME;
import static com.getjavajob.training.web1701.dyominn.common.models.PhoneType.WORK;
import static com.getjavajob.training.web1701.dyominn.common.utils.CryptPassword.hash;
import static java.time.LocalDate.parse;
import static java.util.Arrays.asList;
import static java.util.Collections.singletonList;
import static org.junit.Assert.*;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:spring-dao-test.xml"})
@Transactional
public class AccountDaoTest {

    @Autowired
    private AccountDao accountDao;
    @Autowired
    private GroupDao groupDao;

    private Account account1;
    private Account account2;
    private Account account3;
    private Account account4;
    private Group group1;
    private Group group2;
    private Group group3;

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    public AccountDaoTest() {
        init();
    }

    @Test
    public void getAccountById() {
        Account actual2 = accountDao.get(2);
        assertEquals(account2, actual2);
        assertEquals(account2.getLastName(), actual2.getLastName());
        assertEquals(account2.getFirstName(), actual2.getFirstName());
        assertEquals(account2.getMiddleName(), actual2.getMiddleName());
        assertEquals(account2.getBirthDate(), actual2.getBirthDate());
        assertEquals(account2.getHomeAddress(), actual2.getHomeAddress());
        assertEquals(account2.getWorkAddress(), actual2.getWorkAddress());
        assertEquals(account2.getIcq(), actual2.getIcq());
        assertEquals(account2.getSkype(), actual2.getSkype());
        assertEquals(account2.getAdditionalInfo(), actual2.getAdditionalInfo());
        assertArrayEquals(account2.getAvatar(), actual2.getAvatar());
    }

    @Test
    public void getAccountByIdWithPhones() {
        Account actual4 = accountDao.get(4);
        assertEquals(account4.getWorkPhones(), actual4.getWorkPhones());
        assertEquals(account4.getPersonalPhones(), actual4.getPersonalPhones());
    }

    @Test
    public void getAccountByIdWithFriends() {
        Account actual4 = accountDao.get(4);
        assertEquals(account4.getFriends(), actual4.getFriends());
    }

    @Test
    public void getAccountByIdWithIncomingFriendshipRequests() {
        Account actual3 = accountDao.get(3);
        assertEquals(account3.getIncomingFriendRequests(), actual3.getIncomingFriendRequests());
    }

    @Test
    public void getAccountByIdWithOutcomingFriendshipRequests() {
        Account actual1 = accountDao.get(1);
        assertEquals(account1.getOutcomingFriendRequests(), actual1.getOutcomingFriendRequests());
    }

    @Test
    public void getAccountByIdWithCreatedGroups() {
        Account actual4 = accountDao.get(4);
        assertEquals(account4.getCreatedGroups(), actual4.getCreatedGroups());
    }

    @Test
    public void getAccountByIdWithModeratedGroups() {
        Account actual3 = accountDao.get(3);
        assertEquals(account3.getModeratedGroups(), actual3.getModeratedGroups());
    }

    @Test
    public void getAccountByIdWithGroupSubscriptions() {
        Account actual1 = accountDao.get(1);
        assertEquals(account1.getSubscriptions(), actual1.getSubscriptions());
    }

    @Test
    public void getAccountByIdWithGroupRequests() {
        Account actual1 = accountDao.get(1);
        assertEquals(account1.getRequestedGroups(), actual1.getRequestedGroups());
    }

    @Test
    public void getAccountByInvalidId() {
        assertNull(accountDao.get(-1));
        assertNull(accountDao.get(0));
        assertNull(accountDao.get(1024));
    }

    @Test
    public void getAll() {
        List<Account> expected = new ArrayList<>();
        expected.add(account1);
        expected.add(account2);
        expected.add(account3);
        expected.add(account4);
        List<Account> actual = accountDao.getAll();
        assertEquals(expected, actual);
    }

    @Test
    public void insertAccountModel() {
        // duplicate account to avoid using the same instance in tests
        Account expected = new Account(0, "Last name", "First name", "Middle name", parse("2000-01-01"), "Home", "Work",
                "user@mail.com", "icq", "skype", "additional info", "123", new byte[]{4, 8, 15, 16, 23, 42});
        Account inserted = accountDao.insert(new Account(0, "Last name", "First name", "Middle name", parse("2000-01-01"),
                "Home", "Work", "user@mail.com", "icq", "skype", "additional info", "123", new byte[]{4, 8, 15, 16, 23, 42}));
        Account actual = accountDao.get(inserted.getId());
        assertNotSame(expected, actual);
        assertEquals(expected, actual);
        assertEquals(expected.getLastName(), actual.getLastName());
        assertEquals(expected.getFirstName(), actual.getFirstName());
        assertEquals(expected.getMiddleName(), actual.getMiddleName());
        assertEquals(expected.getBirthDate(), actual.getBirthDate());
        assertEquals(expected.getHomeAddress(), actual.getHomeAddress());
        assertEquals(expected.getWorkAddress(), actual.getWorkAddress());
        assertEquals(expected.getIcq(), actual.getIcq());
        assertEquals(expected.getSkype(), actual.getSkype());
        assertEquals(expected.getAdditionalInfo(), actual.getAdditionalInfo());
        assertArrayEquals(expected.getAvatar(), actual.getAvatar());
    }

    @Test
    public void insertAccountGenerateId() {
        Account inserted = accountDao.insert(new Account(0, "Last name", "First name", "Middle name", parse("2000-01-01"),
                "Home", "Work", "user@mail.com", "icq", "skype", "additional info", "123", new byte[]{4, 8, 15, 16, 23, 42}));
        assertNotEquals(0, inserted.getId());
    }

    @Test
    public void insertAccountWithPhones() {
        // duplicate account to avoid using the same instance in tests
        Account expected = new Account(0, "Last name", "First name", null, null, null, null, "user@mail.com", null,
                null, null, "111", null);
        expected.getWorkPhones().add(new Phone(0, expected, "789", WORK));
        expected.getWorkPhones().add(new Phone(0, expected, "012", WORK));
        expected.getPersonalPhones().add(new Phone(0, expected, "123", HOME));
        expected.getPersonalPhones().add(new Phone(0, expected, "456", HOME));
        Account inserted = new Account(0, "Last name", "First name", null, null, null, null, "user@mail.com", null,
                null, null, "111", null);
        inserted.getWorkPhones().add(new Phone(0, inserted, "789", WORK));
        inserted.getWorkPhones().add(new Phone(0, inserted, "012", WORK));
        inserted.getPersonalPhones().add(new Phone(0, inserted, "123", HOME));
        inserted.getPersonalPhones().add(new Phone(0, inserted, "456", HOME));
        inserted = accountDao.insert(inserted);
        Account actual = accountDao.get(inserted.getId());
        assertNotSame(expected, actual);
        assertEquals(expected.getPersonalPhones(), actual.getPersonalPhones());
        assertEquals(expected.getWorkPhones(), actual.getWorkPhones());
    }

    @Test
    public void insertAccountWithFriends() {
        // duplicate account to avoid using the same instance in tests
        Account expected = new Account(0, "Last name", "First name", null, null, null, null, "user@mail.com", null,
                null, null, "111", null);
        expected.setFriends(new HashSet<>(asList(account1, account2, account3)));
        Account inserted = new Account(0, "Last name", "First name", null, null, null, null, "user@mail.com", null,
                null, null, "111", null);
        inserted.setFriends(new HashSet<>(asList(account1, account2, account3)));
        inserted = accountDao.insert(inserted);
        Account actual = accountDao.get(inserted.getId());
        assertNotSame(expected, actual);
        assertEquals(expected.getFriends(), actual.getFriends());
    }

    @Test
    public void insertAccountWithIncomingFriendRequests() {
        // duplicate account to avoid using the same instance in tests
        Account expected = new Account(0, "Last name", "First name", null, null, null, null, "user@mail.com", null,
                null, null, "111", null);
        expected.setIncomingFriendRequests(new HashSet<>(asList(account1, account2, account3)));
        Account inserted = new Account(0, "Last name", "First name", null, null, null, null, "user@mail.com", null,
                null, null, "111", null);
        inserted.setIncomingFriendRequests(new HashSet<>(asList(account1, account2, account3)));
        inserted = accountDao.insert(inserted);
        Account actual = accountDao.get(inserted.getId());
        assertNotSame(expected, actual);
        assertEquals(expected.getIncomingFriendRequests(), actual.getIncomingFriendRequests());
    }

    @Test
    public void insertAccountWithOutcomingFriendRequests() {
        // duplicate account to avoid using the same instance in tests
        Account expected = new Account(0, "Last name", "First name", null, null, null, null, "user@mail.com", null,
                null, null, "111", null);
        expected.setOutcomingFriendRequests(new HashSet<>(asList(account1, account2, account3)));
        Account inserted = new Account(0, "Last name", "First name", null, null, null, null, "user@mail.com", null,
                null, null, "111", null);
        inserted.setOutcomingFriendRequests(new HashSet<>(asList(account1, account2, account3)));
        inserted = accountDao.insert(inserted);
        Account actual = accountDao.get(inserted.getId());
        assertNotSame(expected, actual);
        assertEquals(expected.getOutcomingFriendRequests(), actual.getOutcomingFriendRequests());
    }

    @Test
    public void insertAccountWithCreatedGroups() {
        // duplicate account to avoid using the same instance in tests
        Account expected = new Account(0, "Last name", "First name", null, null, null, null, "user@mail.com", null,
                null, null, "111", null);
        expected.setCreatedGroups(new ArrayList<>(asList(group1, group2, group3)));
        Account inserted = new Account(0, "Last name", "First name", null, null, null, null, "user@mail.com", null,
                null, null, "111", null);
        inserted.setCreatedGroups(new ArrayList<>(asList(group1, group2, group3)));
        inserted = accountDao.insert(inserted);
        Account actual = accountDao.get(inserted.getId());
        assertNotSame(expected, actual);
        assertEquals(expected.getCreatedGroups(), actual.getCreatedGroups());
    }

    @Test
    public void insertAccountWithModeratedGroups() {
        // duplicate account to avoid using the same instance in tests
        Account expected = new Account(0, "Last name", "First name", null, null, null, null, "user@mail.com", null,
                null, null, "111", null);
        expected.setModeratedGroups(new HashSet<>(asList(group1, group2, group3)));
        Account inserted = new Account(0, "Last name", "First name", null, null, null, null, "user@mail.com", null,
                null, null, "111", null);
        inserted.setModeratedGroups(new HashSet<>(asList(group1, group2, group3)));
        inserted = accountDao.insert(inserted);
        Account actual = accountDao.get(inserted.getId());
        assertNotSame(expected, actual);
        assertEquals(expected.getModeratedGroups(), actual.getModeratedGroups());
    }

    @Test
    public void insertAccountWithGroupsSubscriptions() {
        // duplicate account to avoid using the same instance in tests
        Account expected = new Account(0, "Last name", "First name", null, null, null, null, "user@mail.com", null,
                null, null, "111", null);
        expected.setSubscriptions(new HashSet<>(asList(group1, group2, group3)));
        Account inserted = new Account(0, "Last name", "First name", null, null, null, null, "user@mail.com", null,
                null, null, "111", null);
        inserted.setSubscriptions(new HashSet<>(asList(group1, group2, group3)));
        inserted = accountDao.insert(inserted);
        Account actual = accountDao.get(inserted.getId());
        assertNotSame(expected, actual);
        assertEquals(expected.getSubscriptions(), actual.getSubscriptions());
    }

    @Test
    public void insertAccountWithGroupsRequests() {
        // duplicate account to avoid using the same instance in tests
        Account expected = new Account(0, "Last name", "First name", null, null, null, null, "user@mail.com", null,
                null, null, "111", null);
        expected.setRequestedGroups(new HashSet<>(asList(group1, group2, group3)));
        Account inserted = new Account(0, "Last name", "First name", null, null, null, null, "user@mail.com", null,
                null, null, "111", null);
        inserted.setRequestedGroups(new HashSet<>(asList(group1, group2, group3)));
        inserted = accountDao.insert(inserted);
        Account actual = accountDao.get(inserted.getId());
        assertNotSame(expected, actual);
        assertEquals(expected.getRequestedGroups(), actual.getRequestedGroups());
    }

    @Test
    public void insertAccountWithBusyEmail() {
        Account account = new Account(0, "Last name", "First name", null, null, null, null, "petrov@gmail.com", null,
                null, null, "111", null);
        thrown.expect(PersistenceException.class);
        accountDao.insert(account);
    }

    @Test
    public void updateAccountModel() {
        account1.setLastName("Обновленный");
        account1.setFirstName("Пользователь");
        account1.setMiddleName("Новая фамилия");
        account1.setBirthDate(parse("2050-07-03"));
        account1.setHomeAddress("Новый домашний адрес");
        account1.setWorkAddress("Новый рабочий адрес");
        account1.setEmail("newPost@post.org");
        account1.setIcq("newIcq");
        account1.setSkype("newSkype");
        account1.setAdditionalInfo("Обновленный 1 пользователь");
        account1.setPassword("Новый password");
        account1.setAvatar(new byte[]{4, 8, 15, 16, 23, 42});
        accountDao.update(account1);
        Account updatedAccount1 = accountDao.get(1);
        assertNotSame(account1, updatedAccount1);
        assertEquals(account1, updatedAccount1);
        assertEquals(account1.getLastName(), updatedAccount1.getLastName());
        assertEquals(account1.getFirstName(), updatedAccount1.getFirstName());
        assertEquals(account1.getMiddleName(), updatedAccount1.getMiddleName());
        assertEquals(account1.getBirthDate(), updatedAccount1.getBirthDate());
        assertEquals(account1.getHomeAddress(), updatedAccount1.getHomeAddress());
        assertEquals(account1.getWorkAddress(), updatedAccount1.getWorkAddress());
        assertEquals(account1.getEmail(), updatedAccount1.getEmail());
        assertEquals(account1.getIcq(), updatedAccount1.getIcq());
        assertEquals(account1.getSkype(), updatedAccount1.getSkype());
        assertEquals(account1.getAdditionalInfo(), updatedAccount1.getAdditionalInfo());
        assertEquals(account1.getPassword(), updatedAccount1.getPassword());
        assertArrayEquals(account1.getAvatar(), updatedAccount1.getAvatar());
    }

    @Test
    public void updateAccountWithWorkPhones() {
        account4.getWorkPhones().clear();
        account4.getWorkPhones().add(new Phone(0, account4, "111", WORK));
        account4.getWorkPhones().add(new Phone(0, account4, "222", WORK));
        accountDao.update(account4);
        Account updatedAccount4 = accountDao.get(4);
        assertNotSame(account4, updatedAccount4);
        assertEquals(account4.getWorkPhones(), updatedAccount4.getWorkPhones());
    }

    @Test
    public void updateAccountWithPersonalPhones() {
        account4.getPersonalPhones().clear();
        account4.getPersonalPhones().add(new Phone(0, account4, "333", HOME));
        account4.getPersonalPhones().add(new Phone(0, account4, "444", HOME));
        accountDao.update(account4);
        Account updatedAccount4 = accountDao.get(4);
        assertNotSame(account4, updatedAccount4);
        assertEquals(account4.getPersonalPhones(), updatedAccount4.getPersonalPhones());
    }

    @Test
    public void updateAccountWithFriends() {
        account4.getFriends().clear();
        account4.getFriends().add(account1);
        accountDao.update(account4);
        Account updatedAccount4 = accountDao.get(4);
        assertNotSame(account4, updatedAccount4);
        assertEquals(account4.getFriends(), updatedAccount4.getFriends());
    }

    @Test
    public void updateAccountWithIncomingFriendRequests() {
        account3.getIncomingFriendRequests().clear();
        account3.getIncomingFriendRequests().add(account4);
        accountDao.update(account3);
        Account updatedAccount3 = accountDao.get(3);
        assertNotSame(account3, updatedAccount3);
        assertEquals(account3.getIncomingFriendRequests(), updatedAccount3.getIncomingFriendRequests());
    }

    @Test
    public void updateAccountWithCreatedGroups() {
        account1.getCreatedGroups().clear();
        account1.getCreatedGroups().add(new Group(0, account1, "Test group", "Test description"));
        account1.getCreatedGroups().add(new Group(0, account1, "Test group 2", "Test description 2"));
        accountDao.update(account1);
        Account updatedAccount1 = accountDao.get(1);
        assertNotSame(account1, updatedAccount1);
        assertEquals(account1.getCreatedGroups(), updatedAccount1.getCreatedGroups());
    }

    @Test
    public void updateAccountWithModeratedGroups() {
        account3.getModeratedGroups().clear();
        account3.getModeratedGroups().add(new Group(0, account3, "Test group", "Test description"));
        account3.getModeratedGroups().add(new Group(0, account3, "Test group 2", "Test description 2"));
        accountDao.update(account3);
        Account updatedAccount3 = accountDao.get(3);
        assertNotSame(account3, updatedAccount3);
        assertEquals(account3.getModeratedGroups(), updatedAccount3.getModeratedGroups());
    }

    @Test
    public void updateAccountWithGroupSubscriptions() {
        account1.getSubscriptions().clear();
        account1.getSubscriptions().add(new Group(0, account1, "Test group", "Test description"));
        account1.getSubscriptions().add(new Group(0, account1, "Test group 2", "Test description 2"));
        accountDao.update(account1);
        Account updatedAccount1 = accountDao.get(1);
        assertNotSame(account1, updatedAccount1);
        assertEquals(account1.getSubscriptions(), updatedAccount1.getSubscriptions());
    }

    @Test
    public void updateAccountWithGroupRequests() {
        account1.getRequestedGroups().clear();
        account1.getRequestedGroups().add(new Group(0, account1, "Test group", "Test description"));
        account1.getRequestedGroups().add(new Group(0, account1, "Test group 2", "Test description 2"));
        accountDao.update(account1);
        Account updatedAccount1 = accountDao.get(1);
        assertNotSame(account1, updatedAccount1);
        assertEquals(account1.getRequestedGroups(), updatedAccount1.getRequestedGroups());
    }

    @Test
    public void deleteAccountById() {
        accountDao.delete(1);
        accountDao.delete(4);
        List<Account> expected = new ArrayList<>();
        expected.add(account2);
        expected.add(account3);
        assertEquals(expected, accountDao.getAll());
    }

    @Test
    public void deleteAccountByIdWithCreatedGroups() {
        List<Group> beforeDelete = new ArrayList<>(asList(group1, group2, group3));
        assertEquals(beforeDelete, groupDao.getAll());
        accountDao.delete(4);
        List<Group> expected = new ArrayList<>();
        assertEquals(expected, groupDao.getAll());
    }

    @Test
    public void getAccountForAuthorisationByEmailAndPassword() {
        String email = "user@post";
        String password = "UserPassword0123";
        String hash = hash(password);
        Account expected = accountDao.insert(new Account(0, "User", "User", null, null, null, null, email, null, null,
                null, hash, null));
        Account actual = accountDao.getAccountForAuthorisation(email, password, false);
        assertEquals(expected, actual);
    }

    @Test
    public void getAccountForAuthorisationByEmailAndHash() {
        String email = "user@post";
        String password = "UserPassword0123";
        String hash = hash(password);
        Account expected = accountDao.insert(new Account(0, "User", "User", null, null, null, null, email, null, null,
                null, hash, null));
        Account actual = accountDao.getAccountForAuthorisation(email, hash, true);
        assertEquals(expected, actual);
    }

    @Test
    public void getAccountForAuthorisationByEmailAndWrongPassword() {
        String email = "user@post";
        String password = "UserPassword0123";
        String hash = hash(password);
        accountDao.insert(new Account(0, "User", "User", null, null, null, null, email, null, null, null, hash, null));
        Account actual = accountDao.getAccountForAuthorisation(email, "wrongPassword", false);
        assertNull(actual);
    }

    @Test
    public void getAccountForAuthorisationByEmailAndWrongHash() {
        String email = "user@post";
        String password = "UserPassword0123";
        String hash = hash(password);
        accountDao.insert(new Account(0, "User", "User", null, null, null, null, email, null, null, null, hash, null));
        Account actual = accountDao.getAccountForAuthorisation(email, "123", true);
        assertNull(actual);
    }

    @Test
    public void getAccountForAuthorisationByEmailAndHashAsPassword() {
        String email = "user@post";
        String password = "UserPassword0123";
        String hash = hash(password);
        accountDao.insert(new Account(0, "User", "User", null, null, null, null, email, null, null, null, hash, null));
        Account actual = accountDao.getAccountForAuthorisation(email, hash, false);
        assertNull(actual);
    }

    @Test
    public void getAccountForAuthorisationByEmailAndPasswordAsHash() {
        String email = "user@post";
        String password = "UserPassword0123";
        String hash = hash(password);
        accountDao.insert(new Account(0, "User", "User", null, null, null, null, email, null, null, null, hash, null));
        Account actual = accountDao.getAccountForAuthorisation(email, password, true);
        assertNull(actual);
    }

    @Test
    public void getAccountForAuthorisationWithNotExistingEmail() {
        String email = "not@exist.mail";
        assertNull(accountDao.getAccountForAuthorisation(email, "password", true));
        assertNull(accountDao.getAccountForAuthorisation(email, "password", false));
    }

    @Test
    public void getAllAccountsByNameOrSurnamePattern() {
        List<Account> expected = new ArrayList<>();
        String pattern = "Pattern";
        for (int i = 0; i < 2; i++) {
            String lastName = pattern + i;
            String email = "email_first_case" + i;
            Account account = new Account(0, lastName, "firstName", null, null, null, null, email, null, null, null, "123", null);
            accountDao.insert(account);
            expected.add(account);
        }
        for (int i = 0; i < 2; i++) {
            String firstName = pattern + i;
            String email = "email_second_case" + i;
            Account account = new Account(0, "lastName", firstName, null, null, null, null, email, null, null, null, "123", null);
            accountDao.insert(account);
            expected.add(account);
        }
        for (int i = 0; i < 2; i++) {
            String firstName = pattern + i;
            String lastName = pattern + i;
            String email = "email_third_case" + i;
            Account account = new Account(0, lastName, firstName, null, null, null, null, email, null, null, null, "123", null);
            accountDao.insert(account);
            expected.add(account);
        }
        List<Account> actual = accountDao.getAccountsByNameOrSurnamePattern(pattern);
        assertEquals(expected, actual);
    }

    @Test
    public void getAccountsByCyrillicNameOrSurnamePattern() {
        List<Account> expected = new ArrayList<>(singletonList(account4));
        List<Account> actual = accountDao.getAccountsByNameOrSurnamePattern("Попов");
        assertEquals(expected, actual);
    }

    @Test
    public void getAccountsByEmptyNameOrSurnamePattern() {
        List<Account> expected = new ArrayList<>(asList(account1, account2, account3, account4));
        List<Account> actual = accountDao.getAccountsByNameOrSurnamePattern("");
        assertEquals(expected, actual);
    }

    @Test
    public void getAccountsByNotExistingNameOrSurnamePattern() {
        List<Account> expected = new ArrayList<>();
        List<Account> actual = accountDao.getAccountsByNameOrSurnamePattern("No such pattern");
        assertEquals(expected, actual);
    }

    @Test
    public void getAccountsByNameOrSurnamePatternWithOffset() {
        List<Account> expected = new ArrayList<>();
        String pattern = "Pattern";
        for (int i = 0; i < 3; i++) {
            String lastName = pattern + i;
            String email = "email_first_case" + i;
            Account account = new Account(0, lastName, "firstName", null, null, null, null, email, null, null, null, "123", null);
            accountDao.insert(account);
            expected.add(account);
        }
        for (int i = 0; i < 3; i++) {
            String firstName = pattern + i;
            String email = "email_second_case" + i;
            Account account = new Account(0, "lastName", firstName, null, null, null, null, email, null, null, null, "123", null);
            accountDao.insert(account);
            expected.add(account);
        }
        expected.remove(5);
        expected.remove(0);
        List<Account> actual = accountDao.getAccountsByNameOrSurnamePattern(pattern, 1, 4);
        assertEquals(expected, actual);
    }

    @Test
    public void getAccountsByCyrillicNameOrSurnamePatternWithOffset() {
        List<Account> expected = new ArrayList<>(singletonList(account4));
        List<Account> actual = accountDao.getAccountsByNameOrSurnamePattern("Попов", 0, 1);
        assertEquals(expected, actual);
    }

    @Test
    public void getAccountsByEmptyNameOrSurnamePatternWithOffset() {
        List<Account> expected = new ArrayList<>(asList(account2, account3));
        List<Account> actual = accountDao.getAccountsByNameOrSurnamePattern("", 1, 2);
        assertEquals(expected, actual);
    }

    @Test
    public void getAccountsByNotExistingNameOrSurnamePatternWithOffset() {
        List<Account> expected = new ArrayList<>();
        List<Account> actual = accountDao.getAccountsByNameOrSurnamePattern("No such pattern", 1, 5);
        assertEquals(expected, actual);
    }

    @Test
    public void countAccountsWithNameOrSurnamePattern() {
        String pattern = "Pattern";
        for (int i = 0; i < 3; i++) {
            String lastName = pattern + i;
            String email = "email_first_case" + i;
            Account account = new Account(0, lastName, "firstName", null, null, null, null, email, null, null, null, "123", null);
            accountDao.insert(account);
        }
        for (int i = 0; i < 3; i++) {
            String firstName = pattern + i;
            String email = "email_second_case" + i;
            Account account = new Account(0, "lastName", firstName, null, null, null, null, email, null, null, null, "123", null);
            accountDao.insert(account);
        }
        assertEquals(6, accountDao.countAccountsWithNameOrSurnamePattern(pattern));
    }

    @Test
    public void countAccountsWithNotExistingNameOrSurnamePattern() {
        assertEquals(0, accountDao.countAccountsWithNameOrSurnamePattern("No such pattern"));
    }

    @Test
    public void removeFromFriends() {
        assertTrue(account4.getFriends().contains(account3));
        assertTrue(account3.getFriends().contains(account4));
        accountDao.removeFromFriends(4, 3, account4);
        Account initiator = accountDao.get(4);
        Account removable = accountDao.get(3);
        assertFalse(initiator.getFriends().contains(removable));
        assertFalse(removable.getFriends().contains(initiator));
    }

    @Test
    public void removeFromFriendsAndSendToSubscribers() {
        assertTrue(account4.getFriends().contains(account3));
        assertTrue(account3.getFriends().contains(account4));
        assertFalse(account4.getIncomingFriendRequests().contains(account3));
        accountDao.removeFromFriends(4, 3, account4);
        Account initiator = accountDao.get(4);
        Account removable = accountDao.get(3);
        assertTrue(initiator.getIncomingFriendRequests().contains(removable));
    }

    @Test
    public void removeFromFriendsNotFriendAndNotSubscriber() {
        assertFalse(account4.getFriends().contains(account1));
        assertFalse(account1.getFriends().contains(account4));
        assertFalse(account4.getIncomingFriendRequests().contains(account1));
        assertFalse(account1.getOutcomingFriendRequests().contains(account4));
        accountDao.removeFromFriends(4, 1, account4);
        Account initiator = accountDao.get(4);
        Account notFriend = accountDao.get(1);
        assertFalse(initiator.getFriends().contains(notFriend));
        assertFalse(notFriend.getFriends().contains(initiator));
        assertFalse(initiator.getIncomingFriendRequests().contains(notFriend));
        assertFalse(notFriend.getOutcomingFriendRequests().contains(initiator));
    }

    @Test
    public void removeFromFriendsYourself() {
        accountDao.removeFromFriends(4, 4, account4);
        Account account = accountDao.get(4);
        assertFalse(account.getFriends().contains(account));
        assertFalse(account.getIncomingFriendRequests().contains(account));
        assertFalse(account.getOutcomingFriendRequests().contains(account));
    }

    @Test
    public void removeFromFriendsAndUpdateArgumentInstance() {
        Account removable = accountDao.get(3);
        assertTrue(account4.getFriends().contains(removable));
        assertFalse(account4.getIncomingFriendRequests().contains(removable));
        accountDao.removeFromFriends(4, 3, account4);
        assertFalse(account4.getFriends().contains(removable));
        assertTrue(account4.getIncomingFriendRequests().contains(removable));
    }

    @Test
    public void removeFromFriendsNoFriendNotUpdateArgumentInstance() {
        Account notFriend = accountDao.get(1);
        assertFalse(account4.getFriends().contains(notFriend));
        assertFalse(account4.getIncomingFriendRequests().contains(notFriend));
        accountDao.removeFromFriends(4, 1, account4);
        assertFalse(account4.getFriends().contains(notFriend));
        assertFalse(account4.getIncomingFriendRequests().contains(notFriend));
    }

    @Test
    public void acceptFriendshipRequestAndBecomeFriends() {
        assertFalse(account3.getFriends().contains(account1));
        assertFalse(account1.getFriends().contains(account3));
        accountDao.acceptFriendshipRequest(3, 1, account3);
        Account receiver = accountDao.get(3);
        Account requester = accountDao.get(1);
        assertTrue(receiver.getFriends().contains(requester));
        assertTrue(requester.getFriends().contains(receiver));
    }

    @Test
    public void acceptFriendshipRequestAndRemoveIncomingRequest() {
        assertTrue(account3.getIncomingFriendRequests().contains(account1));
        accountDao.acceptFriendshipRequest(3, 1, account3);
        Account receiver = accountDao.get(3);
        Account requester = accountDao.get(1);
        assertFalse(receiver.getIncomingFriendRequests().contains(requester));
    }

    @Test
    public void acceptFriendshipRequestAndRemoveOutcomingRequest() {
        assertTrue(account1.getOutcomingFriendRequests().contains(account3));
        accountDao.acceptFriendshipRequest(3, 1, account3);
        Account receiver = accountDao.get(3);
        Account requester = accountDao.get(1);
        assertFalse(requester.getOutcomingFriendRequests().contains(receiver));
    }

    @Test
    public void acceptFriendshipRequestFromNotRequester() {
        assertFalse(account4.getFriends().contains(account1));
        assertFalse(account1.getFriends().contains(account4));
        assertFalse(account4.getIncomingFriendRequests().contains(account1));
        assertFalse(account1.getOutcomingFriendRequests().contains(account4));
        accountDao.acceptFriendshipRequest(4, 1, account4);
        Account initiator = accountDao.get(4);
        Account notRequester = accountDao.get(1);
        assertFalse(initiator.getFriends().contains(notRequester));
        assertFalse(notRequester.getFriends().contains(initiator));
        assertFalse(initiator.getIncomingFriendRequests().contains(notRequester));
        assertFalse(notRequester.getOutcomingFriendRequests().contains(initiator));
    }

    @Test
    public void acceptFriendshipRequestFromYourself() {
        accountDao.acceptFriendshipRequest(4, 4, account4);
        Account account = accountDao.get(4);
        assertFalse(account.getFriends().contains(account));
        assertFalse(account.getIncomingFriendRequests().contains(account));
        assertFalse(account.getOutcomingFriendRequests().contains(account));
    }

    @Test
    public void acceptFriendshipRequestAndUpdateArgumentInstance() {
        Account requester = accountDao.get(1);
        assertFalse(account3.getFriends().contains(requester));
        assertTrue(account3.getIncomingFriendRequests().contains(requester));
        accountDao.acceptFriendshipRequest(3, 1, account3);
        assertTrue(account3.getFriends().contains(requester));
        assertFalse(account3.getIncomingFriendRequests().contains(requester));
    }

    @Test
    public void acceptFriendshipRequestFromNotRequesterNotUpdateInstance() {
        Account notRequester = accountDao.get(1);
        assertFalse(account4.getFriends().contains(notRequester));
        assertFalse(account4.getIncomingFriendRequests().contains(notRequester));
        accountDao.acceptFriendshipRequest(4, 1, account4);
        assertFalse(account4.getFriends().contains(notRequester));
        assertFalse(account4.getIncomingFriendRequests().contains(notRequester));
    }

    @Test
    public void declineFriendshipRequestAndRemoveIncomingRequest() {
        assertTrue(account3.getIncomingFriendRequests().contains(account2));
        accountDao.declineFriendshipRequest(3, 2, account3);
        Account receiver = accountDao.get(3);
        Account requester = accountDao.get(2);
        assertFalse(receiver.getIncomingFriendRequests().contains(requester));
    }

    @Test
    public void declineFriendshipRequestAndRemoveOutcomingRequest() {
        assertTrue(account2.getOutcomingFriendRequests().contains(account3));
        accountDao.declineFriendshipRequest(3, 2, account3);
        Account receiver = accountDao.get(3);
        Account requester = accountDao.get(2);
        assertFalse(requester.getOutcomingFriendRequests().contains(receiver));
    }

    @Test
    public void declineFriendshipRequestAndChangeArgumentInstance() {
        Account requester = accountDao.get(2);
        assertTrue(account3.getIncomingFriendRequests().contains(requester));
        accountDao.declineFriendshipRequest(3, 2, account3);
        assertFalse(account3.getIncomingFriendRequests().contains(requester));
    }

    @Test
    public void cancelOutgoingRequestAndRemoveOutcomingRequest() {
        assertTrue(account1.getOutcomingFriendRequests().contains(account2));
        accountDao.cancelOutgoingRequest(1, 2, account1);
        Account requester = accountDao.get(1);
        Account receiver = accountDao.get(2);
        assertFalse(requester.getOutcomingFriendRequests().contains(receiver));
    }

    @Test
    public void cancelOutgoingRequestAndRemoveIncomingRequest() {
        assertTrue(account2.getIncomingFriendRequests().contains(account1));
        accountDao.cancelOutgoingRequest(1, 2, account1);
        Account requester = accountDao.get(1);
        Account receiver = accountDao.get(2);
        assertFalse(receiver.getIncomingFriendRequests().contains(requester));
    }

    @Test
    public void cancelOutgoingRequestAndChangeArgumentInstance() {
        Account receiver = accountDao.get(2);
        assertTrue(account1.getOutcomingFriendRequests().contains(receiver));
        accountDao.cancelOutgoingRequest(1, 2, account1);
        assertFalse(account1.getOutcomingFriendRequests().contains(receiver));
    }

    @Test
    public void sendFriendshipRequestToOutcomingRequests() {
        assertFalse(account1.getOutcomingFriendRequests().contains(account4));
        accountDao.sendFriendshipRequest(1, 4, account1);
        Account requester = accountDao.get(1);
        Account receiver = accountDao.get(4);
        assertTrue(requester.getOutcomingFriendRequests().contains(receiver));
    }

    @Test
    public void sendFriendshipRequestToIncomingRequests() {
        assertFalse(account4.getIncomingFriendRequests().contains(account1));
        accountDao.sendFriendshipRequest(1, 4, account1);
        Account requester = accountDao.get(1);
        Account receiver = accountDao.get(4);
        assertTrue(receiver.getIncomingFriendRequests().contains(requester));
    }

    @Test
    public void sendFriendshipRequestChangeArgumentInstance() {
        Account receiver = accountDao.get(4);
        assertFalse(account1.getOutcomingFriendRequests().contains(receiver));
        accountDao.sendFriendshipRequest(1, 4, account1);
        assertTrue(account1.getOutcomingFriendRequests().contains(receiver));
    }

    @Test
    public void sendFriendshipRequestToYourself() {
        accountDao.sendFriendshipRequest(1, 1, account1);
        Account account = accountDao.get(1);
        assertFalse(account.getOutcomingFriendRequests().contains(account));
        assertFalse(account.getIncomingFriendRequests().contains(account));
    }

    @Test
    public void sendFriendshipRequestToYourselfNotChangeArgumentInstance() {
        Account account = accountDao.get(1);
        assertFalse(account1.getOutcomingFriendRequests().contains(account));
        accountDao.sendFriendshipRequest(1, 1, account1);
        assertFalse(account1.getOutcomingFriendRequests().contains(account));
    }

    @Test
    public void sendFriendshipRequestWhichAlreadyExist() {
        assertTrue(account1.getOutcomingFriendRequests().contains(account2));
        assertTrue(account2.getIncomingFriendRequests().contains(account1));
        accountDao.sendFriendshipRequest(1, 2, account1);
        Account requester = accountDao.get(1);
        Account receiver = accountDao.get(2);
        assertTrue(receiver.getIncomingFriendRequests().contains(requester));
        assertTrue(requester.getOutcomingFriendRequests().contains(receiver));
    }

    @Test
    public void sendFriendshipRequestWhichAlreadyExistNotChangeArgumentInstance() {
        Account receiver = accountDao.get(2);
        assertTrue(account1.getOutcomingFriendRequests().contains(receiver));
        accountDao.sendFriendshipRequest(1, 2, account1);
        assertTrue(account1.getOutcomingFriendRequests().contains(receiver));
    }

    @Test
    public void sendFriendshipRequestWithCounterRequestAndRemoveRequests() {
        assertTrue(account2.getIncomingFriendRequests().contains(account1));
        assertTrue(account1.getOutcomingFriendRequests().contains(account2));
        assertFalse(account2.getOutcomingFriendRequests().contains(account1));
        accountDao.sendFriendshipRequest(2, 1, account2);
        Account requester = accountDao.get(2);
        Account receiver = accountDao.get(1);
        assertFalse(requester.getIncomingFriendRequests().contains(receiver));
        assertFalse(receiver.getOutcomingFriendRequests().contains(requester));
        assertFalse(requester.getOutcomingFriendRequests().contains(receiver));
    }

    @Test
    public void sendFriendshipRequestWithCounterRequestMakeFriends() {
        assertFalse(account1.getFriends().contains(account2));
        assertFalse(account2.getFriends().contains(account1));
        accountDao.sendFriendshipRequest(2, 1, account2);
        Account requester = accountDao.get(2);
        Account receiver = accountDao.get(1);
        assertTrue(requester.getFriends().contains(receiver));
        assertTrue(receiver.getFriends().contains(requester));
    }

    @Test
    public void sendFriendshipRequestWithCounterRequestChangeArgumentInstance() {
        Account receiver = accountDao.get(1);
        assertTrue(account2.getIncomingFriendRequests().contains(receiver));
        assertFalse(account2.getFriends().contains(receiver));
        accountDao.sendFriendshipRequest(2, 1, account2);
        assertFalse(account2.getIncomingFriendRequests().contains(receiver));
        assertTrue(account2.getFriends().contains(receiver));
    }

    @Test
    public void sendGroupRequest() {
        Account account = accountDao.insert(new Account(0, "User", "user", null, null, null, null, "e@mail", null,
                null, null, "123", null));
        accountDao.sendGroupRequest(account, group1);
        Account requester = accountDao.get(account.getId());
        Group group = groupDao.get(1);
        assertTrue(requester.getRequestedGroups().contains(group));
        assertTrue(group.getRequesters().contains(requester));
    }

    @Test
    public void sendGroupRequestChangeAccountArgumentInstance() {
        Account account = accountDao.insert(new Account(0, "User", "user", null, null, null, null, "e@mail", null,
                null, null, "123", null));
        accountDao.sendGroupRequest(account, group1);
        assertTrue(account.getRequestedGroups().contains(group1));
    }

    @Test
    public void sendGroupRequestByCreator() {
        assertTrue(account4.getCreatedGroups().contains(group1));
        accountDao.sendGroupRequest(account4, group1);
        Account requester = accountDao.get(4);
        Group group = groupDao.get(1);
        assertFalse(requester.getRequestedGroups().contains(group));
        assertFalse(group.getRequesters().contains(requester));
    }

    @Test
    public void sendGroupRequestByCreatorNotChangeAccountArgumentInstance() {
        assertTrue(account4.getCreatedGroups().contains(group1));
        accountDao.sendGroupRequest(account4, group1);
        assertFalse(account4.getRequestedGroups().contains(group1));
    }

    @Test
    public void sendGroupRequestByModerator() {
        assertTrue(account3.getModeratedGroups().contains(group2));
        accountDao.sendGroupRequest(account3, group2);
        Account requester = accountDao.get(3);
        Group group = groupDao.get(2);
        assertFalse(requester.getRequestedGroups().contains(group));
        assertFalse(group.getRequesters().contains(requester));
    }

    @Test
    public void sendGroupRequestByModeratorNotChangeAccountArgumentInstance() {
        assertTrue(account3.getModeratedGroups().contains(group2));
        accountDao.sendGroupRequest(account3, group2);
        assertFalse(account3.getRequestedGroups().contains(group2));
    }

    @Test
    public void sendGroupRequestBySubscriber() {
        assertTrue(account2.getSubscriptions().contains(group2));
        accountDao.sendGroupRequest(account2, group2);
        Account requester = accountDao.get(2);
        Group group = groupDao.get(2);
        assertFalse(requester.getRequestedGroups().contains(group));
        assertFalse(group.getRequesters().contains(requester));
    }

    @Test
    public void sendGroupRequestBySubscriberNotChangeAccountArgumentInstance() {
        assertTrue(account2.getSubscriptions().contains(group2));
        accountDao.sendGroupRequest(account2, group2);
        assertFalse(account2.getRequestedGroups().contains(group2));
    }

    @Test
    public void sendGroupRequestByRequester() {
        assertTrue(account1.getRequestedGroups().contains(group1));
        accountDao.sendGroupRequest(account1, group1);
        Account requester = accountDao.get(1);
        Group group = groupDao.get(1);
        assertTrue(requester.getRequestedGroups().contains(group));
        assertTrue(group.getRequesters().contains(requester));
    }

    @Test
    public void sendGroupRequestByRequesterNotChangeAccountArgumentInstance() {
        assertTrue(account1.getRequestedGroups().contains(group1));
        accountDao.sendGroupRequest(account1, group1);
        assertTrue(account1.getRequestedGroups().contains(group1));
    }

    @Test
    public void cancelRequestToGroupByRequester() {
        assertTrue(account1.getRequestedGroups().contains(group1));
        assertTrue(group1.getRequesters().contains(account1));
        accountDao.cancelRequestToGroup(account1, group1);
        Account requester = accountDao.get(1);
        assertFalse(requester.getRequestedGroups().contains(group1));
        assertFalse(group1.getRequesters().contains(requester));
    }

    @Test
    public void cancelRequestToGroupByRequesterChangeAccountArgumentInstance() {
        assertTrue(account1.getRequestedGroups().contains(group1));
        accountDao.cancelRequestToGroup(account1, group1);
        assertFalse(account1.getRequestedGroups().contains(group1));
    }

    @Test
    public void cancelRequestToGroupWithoutRequest() {
        assertFalse(account3.getRequestedGroups().contains(group1));
        assertFalse(group1.getRequesters().contains(account3));
        accountDao.cancelRequestToGroup(account3, group1);
        Account requester = accountDao.get(3);
        Group group = groupDao.get(1);
        assertFalse(requester.getRequestedGroups().contains(group));
        assertFalse(group.getRequesters().contains(requester));
    }

    @Test
    public void cancelRequestToGroupWithoutRequestNotChangeArgumentInstance() {
        assertFalse(account3.getRequestedGroups().contains(group1));
        accountDao.cancelRequestToGroup(account3, group1);
        assertFalse(account3.getRequestedGroups().contains(group1));
    }

    @Test
    public void leaveGroupByModerator() {
        assertTrue(account3.getModeratedGroups().contains(group3));
        assertTrue(group3.getModerators().contains(account3));
        accountDao.leaveGroup(account3, group3);
        Account moderator = accountDao.get(3);
        assertFalse(moderator.getModeratedGroups().contains(group3));
        assertFalse(moderator.getSubscriptions().contains(group3));
        assertFalse(moderator.getRequestedGroups().contains(group3));
        assertFalse(group3.getModerators().contains(moderator));
        assertFalse(group3.getSubscribers().contains(moderator));
        assertFalse(group3.getRequesters().contains(moderator));
    }

    @Test
    public void leaveGroupByModeratorChangeAccountArgumentInstance() {
        assertTrue(account3.getModeratedGroups().contains(group3));
        accountDao.leaveGroup(account3, group3);
        assertFalse(account3.getModeratedGroups().contains(group3));
        assertFalse(account3.getSubscriptions().contains(group3));
        assertFalse(account3.getRequestedGroups().contains(group3));
    }

    @Test
    public void leaveGroupBySubscriber() {
        assertTrue(account1.getSubscriptions().contains(group3));
        assertTrue(group3.getSubscribers().contains(account1));
        accountDao.leaveGroup(account1, group3);
        Account subscriber = accountDao.get(1);
        assertFalse(subscriber.getModeratedGroups().contains(group3));
        assertFalse(subscriber.getSubscriptions().contains(group3));
        assertFalse(subscriber.getRequestedGroups().contains(group3));
        assertFalse(group3.getModerators().contains(subscriber));
        assertFalse(group3.getSubscribers().contains(subscriber));
        assertFalse(group3.getRequesters().contains(subscriber));
    }

    @Test
    public void leaveGroupBySubscriberChangeAccountArgumentInstance() {
        assertTrue(account1.getSubscriptions().contains(group3));
        accountDao.leaveGroup(account1, group3);
        assertFalse(account1.getModeratedGroups().contains(group3));
        assertFalse(account1.getSubscriptions().contains(group3));
        assertFalse(account1.getRequestedGroups().contains(group3));
    }

    @Test
    public void leaveGroupByRequesterChangeNothing() {
        assertTrue(account1.getRequestedGroups().contains(group1));
        assertTrue(group1.getRequesters().contains(account1));
        accountDao.leaveGroup(account1, group1);
        Account requester = accountDao.get(1);
        Group group = groupDao.get(1);
        assertFalse(requester.getModeratedGroups().contains(group));
        assertFalse(requester.getSubscriptions().contains(group));
        assertTrue(requester.getRequestedGroups().contains(group));
        assertFalse(group.getModerators().contains(requester));
        assertFalse(group.getSubscribers().contains(requester));
        assertTrue(group.getRequesters().contains(requester));
    }

    @Test
    public void leaveGroupByRequesterNotChangeAccountArgumentInstance() {
        assertTrue(account1.getRequestedGroups().contains(group1));
        accountDao.leaveGroup(account1, group1);
        assertFalse(account1.getModeratedGroups().contains(group1));
        assertFalse(account1.getSubscriptions().contains(group1));
        assertTrue(account1.getRequestedGroups().contains(group1));
    }

    @Test
    public void leaveGroupByNotRelatedAccount() {
        Account account = accountDao.insert(new Account(0, "User", "user", null, null, null, null, "e@mail", null,
                null, null, "123", null));
        accountDao.leaveGroup(account, group1);
        Account managedAccount = accountDao.get(account.getId());
        assertFalse(managedAccount.getModeratedGroups().contains(group1));
        assertFalse(managedAccount.getSubscriptions().contains(group1));
        assertFalse(managedAccount.getRequestedGroups().contains(group1));
    }

    @Test
    public void leaveGroupByNotRelatedNotChangeAccountArgumentInstance() {
        Account account = accountDao.insert(new Account(0, "User", "user", null, null, null, null, "e@mail", null,
                null, null, "123", null));
        accountDao.leaveGroup(account, group1);
        assertFalse(account.getModeratedGroups().contains(group1));
        assertFalse(account.getSubscriptions().contains(group1));
        assertFalse(account.getRequestedGroups().contains(group1));
    }

    private void init() {
        account1 = new Account(1, "Ivanov", "Ivan", null, null, null, null, "ivanov@gmail.com", null, null, null, "111", null);
        account2 = new Account(2, "Petrov", "Petr", "Petrovich", parse("2000-01-01"),
                "Russia, Moscow, Red Square", "Russia, Moscow, Kremlin", "petrov@gmail.com", "petrov_icq", "petrov_skype",
                "petrov_additional_info", "123", null);
        account3 = new Account(3, "Petrov", "Petr", "Petrovich", parse("2000-01-01"),
                "Russia, Moscow, Red Square", "Russia, Moscow, Kremlin", "petrov_copy@gmail.com", "petrov_icq", "petrov_skype",
                "Copy of Petrov with another e-mail", "123", null);
        account4 = new Account(4, "Попова", "Ольга", "Викторовна", parse("1990-12-20"),
                "Россия, г. Омск, ул. Центральная, д. 10-12", "Домохозяйка", "popova@yandex.ru", "popova_icq",
                "popova_skype", "Пользователь, записанный кириллицей", "12345", null);

        group1 = new Group(1, account4, "Empty group", null);
        group2 = new Group(2, account4, "Java", "Java programming language");
        group3 = new Group(3, account4, "Космос", "Космос в картинках и фактах");

        Phone phone1 = new Phone(1, account3, "11111111111", HOME);
        Phone phone2 = new Phone(2, account3, "22222222222", WORK);
        Phone phone3 = new Phone(3, account4, "44444444444", HOME);
        Phone phone4 = new Phone(4, account4, "55555555555", HOME);
        Phone phone5 = new Phone(5, account4, "66666666666", WORK);
        Phone phone6 = new Phone(6, account4, "22222222222", WORK);

        account3.setPersonalPhones(new ArrayList<>(singletonList(phone1)));
        account3.setWorkPhones(new ArrayList<>(singletonList(phone2)));
        account4.setPersonalPhones(new ArrayList<>(asList(phone3, phone4)));
        account4.setWorkPhones(new ArrayList<>(asList(phone5, phone6)));

        account2.setFriends(new HashSet<>(singletonList(account4)));
        account3.setFriends(new HashSet<>(singletonList(account4)));
        account4.setFriends(new HashSet<>(asList(account2, account3)));
        account1.setOutcomingFriendRequests(new HashSet<>(asList(account2, account3)));
        account2.setOutcomingFriendRequests(new HashSet<>(singletonList(account3)));
        account2.setIncomingFriendRequests(new HashSet<>(singletonList(account1)));
        account3.setIncomingFriendRequests(new HashSet<>(asList(account1, account2)));

        account4.setCreatedGroups(new ArrayList<>(asList(group1, group2, group3)));
        account2.setModeratedGroups(new HashSet<>(singletonList(group3)));
        account3.setModeratedGroups(new HashSet<>(asList(group2, group3)));
        account1.setSubscriptions(new HashSet<>(asList(group2, group3)));
        account2.setSubscriptions(new HashSet<>(singletonList(group2)));
        account3.setSubscriptions(new HashSet<>(singletonList(group1)));
        account1.setRequestedGroups(new HashSet<>(singletonList(group1)));
        account2.setRequestedGroups(new HashSet<>(singletonList(group1)));

        group1.setModerators(new HashSet<>());
        group1.setSubscribers(new HashSet<>(singletonList(account3)));
        group1.setRequesters(new HashSet<>(asList(account1, account2)));
        group2.setModerators(new HashSet<>(singletonList(account3)));
        group2.setSubscribers(new HashSet<>(asList(account1, account2)));
        group2.setRequesters(new HashSet<>());
        group3.setModerators(new HashSet<>(asList(account2, account3)));
        group3.setSubscribers(new HashSet<>(singletonList(account1)));
        group3.setRequesters(new HashSet<>());
    }
}