package com.getjavajob.training.web1701.dyominn.dao;

import com.getjavajob.training.web1701.dyominn.common.models.Account;
import com.getjavajob.training.web1701.dyominn.common.models.Group;
import com.getjavajob.training.web1701.dyominn.common.models.Phone;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.PersistenceException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import static com.getjavajob.training.web1701.dyominn.common.models.PhoneType.HOME;
import static com.getjavajob.training.web1701.dyominn.common.models.PhoneType.WORK;
import static java.time.LocalDate.parse;
import static java.util.Arrays.asList;
import static java.util.Collections.singletonList;
import static org.junit.Assert.*;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:spring-dao-test.xml"})
@Transactional
public class GroupDaoTest {

    @Autowired
    private AccountDao accountDao;
    @Autowired
    private GroupDao groupDao;

    private Account account1;
    private Account account2;
    private Account account3;
    private Account account4;
    private Group group1;
    private Group group2;
    private Group group3;

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    public GroupDaoTest() {
        init();
    }

    @Test
    public void getGroupById() {
        Group actual3 = groupDao.get(3);
        assertEquals(group3, actual3);
        assertEquals(group3.getName(), actual3.getName());
        assertEquals(group3.getDescription(), actual3.getDescription());
        assertEquals(group3.getCreator(), actual3.getCreator());
        assertArrayEquals(group3.getAvatar(), actual3.getAvatar());
    }

    @Test
    public void getGroupByIdWithModerators() {
        Group actual3 = groupDao.get(3);
        assertEquals(group3.getModerators(), actual3.getModerators());
    }

    @Test
    public void getGroupByIdWithSubscribers() {
        Group actual3 = groupDao.get(3);
        assertEquals(group3.getSubscribers(), actual3.getSubscribers());
    }

    @Test
    public void getGroupByIdWithRequesters() {
        Group actual3 = groupDao.get(3);
        assertEquals(group3.getRequesters(), actual3.getRequesters());
    }

    @Test
    public void getGroupByInvalidId() {
        assertNull(groupDao.get(-1));
        assertNull(groupDao.get(0));
        assertNull(groupDao.get(1024));
    }

    @Test
    public void getAll() {
        List<Group> expected = new ArrayList<>();
        expected.add(group1);
        expected.add(group2);
        expected.add(group3);
        List<Group> actual = groupDao.getAll();
        assertEquals(expected, actual);
    }

    @Test
    public void insertGroupModel() {
        Group expected = new Group(0, account1, "testGroup", "testDescription");
        expected.setAvatar(new byte[]{4, 8, 15, 16, 23, 42});
        Group inserted = new Group(0, account1, "testGroup", "testDescription");
        inserted.setAvatar(new byte[]{4, 8, 15, 16, 23, 42});
        inserted = groupDao.insert(inserted);
        Group actual = groupDao.get(inserted.getId());
        assertNotSame(expected, actual);
        assertEquals(expected, actual);
        assertEquals(expected.getName(), actual.getName());
        assertEquals(expected.getDescription(), actual.getDescription());
        assertEquals(expected.getCreator(), actual.getCreator());
        assertArrayEquals(expected.getAvatar(), actual.getAvatar());
    }

    @Test
    public void insertGroupGenerateId() {
        Group inserted = groupDao.insert(new Group(0, account1, "testGroup", "testDescription"));
        assertNotEquals(0, inserted.getId());
    }

    @Test
    public void insertGroupWithModerators() {
        Group expected = new Group(0, account1, "testGroup", "testDescription");
        expected.setModerators(new HashSet<>(asList(account2, account3)));
        Group inserted = new Group(0, account1, "testGroup", "testDescription");
        inserted.setModerators(new HashSet<>(asList(account2, account3)));
        inserted = groupDao.insert(inserted);
        Group actual = groupDao.get(inserted.getId());
        assertNotSame(expected.getModerators(), actual.getModerators());
        assertEquals(expected.getModerators(), actual.getModerators());
    }

    @Test
    public void insertGroupWithSubscribers() {
        Group expected = new Group(0, account1, "testGroup", "testDescription");
        expected.setSubscribers(new HashSet<>(asList(account3, account4)));
        Group inserted = new Group(0, account1, "testGroup", "testDescription");
        inserted.setSubscribers(new HashSet<>(asList(account3, account4)));
        inserted = groupDao.insert(inserted);
        Group actual = groupDao.get(inserted.getId());
        assertNotSame(expected.getSubscribers(), actual.getSubscribers());
        assertEquals(expected.getSubscribers(), actual.getSubscribers());
    }

    @Test
    public void insertGroupWithRequesters() {
        Group expected = new Group(0, account1, "testGroup", "testDescription");
        expected.setRequesters(new HashSet<>(asList(account2, account4)));
        Group inserted = new Group(0, account1, "testGroup", "testDescription");
        inserted.setRequesters(new HashSet<>(asList(account2, account4)));
        inserted = groupDao.insert(inserted);
        Group actual = groupDao.get(inserted.getId());
        assertNotSame(expected.getRequesters(), actual.getRequesters());
        assertEquals(expected.getRequesters(), actual.getRequesters());
    }

    @Test
    public void insertGroupWithBusyName() {
        Group group = new Group(2, account4, "Java", "Java programming language");
        thrown.expect(PersistenceException.class);
        groupDao.insert(group);
    }

    @Test
    public void updateGroupModel() {
        group1.setCreator(account1);
        group1.setName("Updated name");
        group1.setDescription("Updated description");
        group1.setAvatar(new byte[]{1, 2, 3, 4, 5});
        groupDao.update(group1);
        Group updatedGroup1 = groupDao.get(1);
        assertNotSame(group1, updatedGroup1);
        assertEquals(group1.getCreator(), updatedGroup1.getCreator());
        assertEquals(group1.getName(), updatedGroup1.getName());
        assertEquals(group1.getDescription(), updatedGroup1.getDescription());
        assertArrayEquals(group1.getAvatar(), updatedGroup1.getAvatar());
    }

    @Test
    public void deleteGroupById() {
        groupDao.delete(1);
        groupDao.delete(3);
        List<Group> expected = new ArrayList<>();
        expected.add(group2);
        assertEquals(expected, groupDao.getAll());
    }

    @Test
    public void deleteGroupNotAffectCreatorAccount() {
        assertTrue(group1.getCreator().equals(account4));
        groupDao.delete(1);
        assertNotNull(accountDao.get(4));
    }

    @Test
    public void deleteGroupNotAffectModeratorsAccounts() {
        assertTrue(group2.getModerators().contains(account3));
        groupDao.delete(2);
        assertNotNull(accountDao.get(3));
    }

    @Test
    public void deleteGroupNotAffectSubscribersAccounts() {
        assertTrue(group1.getSubscribers().contains(account3));
        groupDao.delete(1);
        assertNotNull(accountDao.get(3));
    }

    @Test
    public void deleteGroupNotAffectRequestersAccounts() {
        assertTrue(group1.getRequesters().contains(account1));
        assertTrue(group1.getRequesters().contains(account2));
        groupDao.delete(1);
        assertNotNull(accountDao.get(1));
        assertNotNull(accountDao.get(2));
    }

    @Test
    public void removeGroupMemberModeratorFromGroupSide() {
        assertTrue(group3.getModerators().contains(account2));
        assertFalse(group3.getSubscribers().contains(account2));
        assertFalse(group3.getRequesters().contains(account2));
        groupDao.removeGroupMember(group3, account2);
        Group group = groupDao.get(3);
        assertFalse(group.getModerators().contains(account2));
        assertFalse(group.getSubscribers().contains(account2));
        assertFalse(group.getRequesters().contains(account2));
    }

    @Test
    public void removeGroupMemberModeratorChangeGroupArgumentInstance() {
        assertTrue(group3.getModerators().contains(account2));
        assertFalse(group3.getSubscribers().contains(account2));
        assertFalse(group3.getRequesters().contains(account2));
        groupDao.removeGroupMember(group3, account2);
        assertFalse(group3.getModerators().contains(account2));
        assertFalse(group3.getSubscribers().contains(account2));
        assertFalse(group3.getRequesters().contains(account2));
    }

    @Test
    public void removeGroupMemberModeratorFromAccountSide() {
        assertTrue(account2.getModeratedGroups().contains(group3));
        assertFalse(account2.getSubscriptions().contains(group3));
        assertFalse(account2.getRequestedGroups().contains(group3));
        groupDao.removeGroupMember(group3, account2);
        Account account = accountDao.get(2);
        assertFalse(account.getModeratedGroups().contains(group3));
        assertFalse(account.getSubscriptions().contains(group3));
        assertFalse(account.getRequestedGroups().contains(group3));
    }

    @Test
    public void removeGroupMemberModeratorChangeAccountArgumentInstance() {
        assertTrue(account2.getModeratedGroups().contains(group3));
        assertFalse(account2.getSubscriptions().contains(group3));
        assertFalse(account2.getRequestedGroups().contains(group3));
        groupDao.removeGroupMember(group3, account2);
        assertFalse(account2.getModeratedGroups().contains(group3));
        assertFalse(account2.getSubscriptions().contains(group3));
        assertFalse(account2.getRequestedGroups().contains(group3));
    }

    @Test
    public void removeGroupMemberSubscriberFromGroupSide() {
        assertFalse(group2.getModerators().contains(account2));
        assertTrue(group2.getSubscribers().contains(account2));
        assertFalse(group2.getRequesters().contains(account2));
        groupDao.removeGroupMember(group2, account2);
        Group group = groupDao.get(2);
        assertFalse(group.getModerators().contains(account2));
        assertFalse(group.getSubscribers().contains(account2));
        assertFalse(group.getRequesters().contains(account2));
    }

    @Test
    public void removeGroupMemberSubscriberChangeGroupArgumentInstance() {
        assertFalse(group2.getModerators().contains(account2));
        assertTrue(group2.getSubscribers().contains(account2));
        assertFalse(group2.getRequesters().contains(account2));
        groupDao.removeGroupMember(group2, account2);
        assertFalse(group2.getModerators().contains(account2));
        assertFalse(group2.getSubscribers().contains(account2));
        assertFalse(group2.getRequesters().contains(account2));
    }

    @Test
    public void removeGroupMemberSubscriberFromAccountSide() {
        assertFalse(account2.getModeratedGroups().contains(group2));
        assertTrue(account2.getSubscriptions().contains(group2));
        assertFalse(account2.getRequestedGroups().contains(group2));
        groupDao.removeGroupMember(group2, account2);
        Account account = accountDao.get(2);
        assertFalse(account.getModeratedGroups().contains(group2));
        assertFalse(account.getSubscriptions().contains(group2));
        assertFalse(account.getRequestedGroups().contains(group2));
    }

    @Test
    public void removeGroupMemberSubscriberChangeAccountArgumentInstance() {
        assertFalse(account2.getModeratedGroups().contains(group2));
        assertTrue(account2.getSubscriptions().contains(group2));
        assertFalse(account2.getRequestedGroups().contains(group2));
        groupDao.removeGroupMember(group2, account2);
        assertFalse(account2.getModeratedGroups().contains(group2));
        assertFalse(account2.getSubscriptions().contains(group2));
        assertFalse(account2.getRequestedGroups().contains(group2));
    }

    @Test
    public void removeGroupMemberRequesterChangeNothingAtGroupSide() {
        assertFalse(group1.getModerators().contains(account2));
        assertFalse(group1.getSubscribers().contains(account2));
        assertTrue(group1.getRequesters().contains(account2));
        groupDao.removeGroupMember(group1, account2);
        Group group = groupDao.get(1);
        assertFalse(group.getModerators().contains(account2));
        assertFalse(group.getSubscribers().contains(account2));
        assertTrue(group.getRequesters().contains(account2));
    }

    @Test
    public void removeGroupMemberRequesterNotChangeGroupArgumentInstance() {
        assertFalse(group1.getModerators().contains(account2));
        assertFalse(group1.getSubscribers().contains(account2));
        assertTrue(group1.getRequesters().contains(account2));
        groupDao.removeGroupMember(group1, account2);
        assertFalse(group1.getModerators().contains(account2));
        assertFalse(group1.getSubscribers().contains(account2));
        assertTrue(group1.getRequesters().contains(account2));
    }

    @Test
    public void removeGroupMemberRequesterChangeNothingAtAccountSide() {
        assertFalse(account2.getModeratedGroups().contains(group1));
        assertFalse(account2.getSubscriptions().contains(group1));
        assertTrue(account2.getRequestedGroups().contains(group1));
        groupDao.removeGroupMember(group1, account2);
        Account account = accountDao.get(2);
        assertFalse(account.getModeratedGroups().contains(group1));
        assertFalse(account.getSubscriptions().contains(group1));
        assertTrue(account.getRequestedGroups().contains(group1));
    }

    @Test
    public void removeGroupMemberRequesterNotChangeAccountArgumentInstance() {
        assertFalse(account2.getModeratedGroups().contains(group1));
        assertFalse(account2.getSubscriptions().contains(group1));
        assertTrue(account2.getRequestedGroups().contains(group1));
        groupDao.removeGroupMember(group1, account2);
        assertFalse(account2.getModeratedGroups().contains(group1));
        assertFalse(account2.getSubscriptions().contains(group1));
        assertTrue(account2.getRequestedGroups().contains(group1));
    }

    @Test
    public void dismissFromModeratorGroupSide() {
        assertTrue(group2.getModerators().contains(account3));
        assertFalse(group2.getSubscribers().contains(account3));
        groupDao.dismissFromModerator(group2, account3);
        Group group = groupDao.get(2);
        assertFalse(group.getModerators().contains(account3));
        assertTrue(group.getSubscribers().contains(account3));
    }

    @Test
    public void dismissFromModeratorChangeGroupArgumentInstance() {
        assertTrue(group2.getModerators().contains(account3));
        assertFalse(group2.getSubscribers().contains(account3));
        groupDao.dismissFromModerator(group2, account3);
        assertFalse(group2.getModerators().contains(account3));
        assertTrue(group2.getSubscribers().contains(account3));
    }

    @Test
    public void dismissFromModeratorAccountSide() {
        assertTrue(account3.getModeratedGroups().contains(group2));
        assertFalse(account3.getSubscriptions().contains(group2));
        groupDao.dismissFromModerator(group2, account3);
        Account account = accountDao.get(3);
        assertFalse(account.getModeratedGroups().contains(group2));
        assertTrue(account.getSubscriptions().contains(group2));
    }

    @Test
    public void dismissFromModeratorChangeAccountArgumentInstance() {
        assertTrue(account3.getModeratedGroups().contains(group2));
        assertFalse(account3.getSubscriptions().contains(group2));
        groupDao.dismissFromModerator(group2, account3);
        assertFalse(account3.getModeratedGroups().contains(group2));
        assertTrue(account3.getSubscriptions().contains(group2));
    }

    @Test
    public void dismissFromModeratorWithSubscriberChangeNothingAtGroupSide() {
        assertFalse(group1.getModerators().contains(account3));
        assertTrue(group1.getSubscribers().contains(account3));
        groupDao.dismissFromModerator(group1, account3);
        Group group = groupDao.get(1);
        assertFalse(group.getModerators().contains(account3));
        assertTrue(group.getSubscribers().contains(account3));
    }

    @Test
    public void dismissFromModeratorWithSubscriberNotChangeGroupArgumentInstance() {
        assertFalse(group1.getModerators().contains(account3));
        assertTrue(group1.getSubscribers().contains(account3));
        groupDao.dismissFromModerator(group1, account3);
        assertFalse(group1.getModerators().contains(account3));
        assertTrue(group1.getSubscribers().contains(account3));
    }

    @Test
    public void dismissFromModeratorWithSubscriberChangeNothingAtAccountSide() {
        assertFalse(account3.getModeratedGroups().contains(group1));
        assertTrue(account3.getSubscriptions().contains(group1));
        groupDao.dismissFromModerator(group1, account3);
        Account account = accountDao.get(3);
        assertFalse(account.getModeratedGroups().contains(group1));
        assertTrue(account.getSubscriptions().contains(group1));
    }

    @Test
    public void dismissFromModeratorWithSubscriberNotChangeAccountArgumentInstance() {
        assertFalse(account3.getModeratedGroups().contains(group1));
        assertTrue(account3.getSubscriptions().contains(group1));
        groupDao.dismissFromModerator(group1, account3);
        assertFalse(account3.getModeratedGroups().contains(group1));
        assertTrue(account3.getSubscriptions().contains(group1));
    }

    @Test
    public void dismissFromModeratorWithRequesterChangeNothingAtGroupSide() {
        assertFalse(group1.getModerators().contains(account1));
        assertFalse(group1.getSubscribers().contains(account1));
        assertTrue(group1.getRequesters().contains(account1));
        groupDao.dismissFromModerator(group1, account1);
        Group group = groupDao.get(1);
        assertFalse(group.getModerators().contains(account1));
        assertFalse(group.getSubscribers().contains(account1));
        assertTrue(group.getRequesters().contains(account1));
    }

    @Test
    public void dismissFromModeratorWithRequesterNotChangeGroupArgumentInstance() {
        assertFalse(group1.getModerators().contains(account1));
        assertFalse(group1.getSubscribers().contains(account1));
        assertTrue(group1.getRequesters().contains(account1));
        groupDao.dismissFromModerator(group1, account1);
        assertFalse(group1.getModerators().contains(account1));
        assertFalse(group1.getSubscribers().contains(account1));
        assertTrue(group1.getRequesters().contains(account1));
    }

    @Test
    public void dismissFromModeratorWithRequesterChangeNothingAtAccountSide() {
        assertFalse(account1.getModeratedGroups().contains(group1));
        assertFalse(account1.getSubscriptions().contains(group1));
        assertTrue(account1.getRequestedGroups().contains(group1));
        groupDao.dismissFromModerator(group1, account1);
        Account account = accountDao.get(1);
        assertFalse(account.getModeratedGroups().contains(group1));
        assertFalse(account.getSubscriptions().contains(group1));
        assertTrue(account.getRequestedGroups().contains(group1));
    }

    @Test
    public void dismissFromModeratorWithRequesterNotChangeAccountArgumentInstance() {
        assertFalse(account1.getModeratedGroups().contains(group1));
        assertFalse(account1.getSubscriptions().contains(group1));
        assertTrue(account1.getRequestedGroups().contains(group1));
        groupDao.dismissFromModerator(group1, account1);
        assertFalse(account1.getModeratedGroups().contains(group1));
        assertFalse(account1.getSubscriptions().contains(group1));
        assertTrue(account1.getRequestedGroups().contains(group1));
    }

    @Test
    public void promoteToModeratorWithSubscriberGroupSide() {
        assertTrue(group1.getSubscribers().contains(account3));
        assertFalse(group1.getModerators().contains(account3));
        groupDao.promoteToModerator(group1, account3);
        Group group = groupDao.get(1);
        assertFalse(group.getSubscribers().contains(account3));
        assertTrue(group.getModerators().contains(account3));
    }

    @Test
    public void promoteToModeratorWithSubscriberChangeGroupArgumentInstance() {
        assertTrue(group1.getSubscribers().contains(account3));
        assertFalse(group1.getModerators().contains(account3));
        groupDao.promoteToModerator(group1, account3);
        assertFalse(group1.getSubscribers().contains(account3));
        assertTrue(group1.getModerators().contains(account3));
    }

    @Test
    public void promoteToModeratorWithSubscriberAccountSide() {
        assertTrue(account3.getSubscriptions().contains(group1));
        assertFalse(account3.getModeratedGroups().contains(group1));
        groupDao.promoteToModerator(group1, account3);
        Account account = accountDao.get(3);
        assertFalse(account.getSubscriptions().contains(group1));
        assertTrue(account.getModeratedGroups().contains(group1));
    }

    @Test
    public void promoteToModeratorWithSubscriberChangeAccountArgumentInstance() {
        assertTrue(account3.getSubscriptions().contains(group1));
        assertFalse(account3.getModeratedGroups().contains(group1));
        groupDao.promoteToModerator(group1, account3);
        assertFalse(account3.getSubscriptions().contains(group1));
        assertTrue(account3.getModeratedGroups().contains(group1));
    }

    @Test
    public void promoteToModeratorWithModeratorChangeNothingAtGroupSide() {
        assertFalse(group2.getSubscribers().contains(account3));
        assertTrue(group2.getModerators().contains(account3));
        groupDao.promoteToModerator(group2, account3);
        Group group = groupDao.get(2);
        assertFalse(group.getSubscribers().contains(account3));
        assertTrue(group.getModerators().contains(account3));
    }

    @Test
    public void promoteToModeratorWithModeratorNotChangeGroupArgumentInstance() {
        assertFalse(group2.getSubscribers().contains(account3));
        assertTrue(group2.getModerators().contains(account3));
        groupDao.promoteToModerator(group2, account3);
        assertFalse(group2.getSubscribers().contains(account3));
        assertTrue(group2.getModerators().contains(account3));
    }

    @Test
    public void promoteToModeratorWithModeratorChangeNothingAtAccountSide() {
        assertFalse(account3.getSubscriptions().contains(group2));
        assertTrue(account3.getModeratedGroups().contains(group2));
        groupDao.promoteToModerator(group2, account3);
        Account account = accountDao.get(3);
        assertFalse(account.getSubscriptions().contains(group2));
        assertTrue(account.getModeratedGroups().contains(group2));
    }

    @Test
    public void promoteToModeratorWithModeratorNotChangeAccountArgumentInstance() {
        assertFalse(account3.getSubscriptions().contains(group2));
        assertTrue(account3.getModeratedGroups().contains(group2));
        groupDao.promoteToModerator(group2, account3);
        assertFalse(account3.getSubscriptions().contains(group2));
        assertTrue(account3.getModeratedGroups().contains(group2));
    }

    @Test
    public void promoteToModeratorWithRequesterChangeNothingAtGroupSide() {
        assertFalse(group1.getSubscribers().contains(account1));
        assertFalse(group1.getModerators().contains(account1));
        assertTrue(group1.getRequesters().contains(account1));
        groupDao.promoteToModerator(group1, account1);
        Group group = groupDao.get(1);
        assertFalse(group.getSubscribers().contains(account1));
        assertFalse(group.getModerators().contains(account1));
        assertTrue(group.getRequesters().contains(account1));
    }

    @Test
    public void promoteToModeratorWithRequesterNotChangeGroupArgumentInstance() {
        assertFalse(group1.getSubscribers().contains(account1));
        assertFalse(group1.getModerators().contains(account1));
        assertTrue(group1.getRequesters().contains(account1));
        groupDao.promoteToModerator(group1, account1);
        assertFalse(group1.getSubscribers().contains(account1));
        assertFalse(group1.getModerators().contains(account1));
        assertTrue(group1.getRequesters().contains(account1));
    }

    @Test
    public void promoteToModeratorWithRequesterChangeNothingAtAccountSide() {
        assertFalse(account1.getSubscriptions().contains(group1));
        assertFalse(account1.getModeratedGroups().contains(group1));
        assertTrue(account1.getRequestedGroups().contains(group1));
        groupDao.promoteToModerator(group1, account1);
        Account account = accountDao.get(1);
        assertFalse(account.getSubscriptions().contains(group1));
        assertFalse(account.getModeratedGroups().contains(group1));
        assertTrue(account.getRequestedGroups().contains(group1));
    }

    @Test
    public void promoteToModeratorWithRequesterNotChangeAccountArgumentInstance() {
        assertFalse(account1.getSubscriptions().contains(group1));
        assertFalse(account1.getModeratedGroups().contains(group1));
        assertTrue(account1.getRequestedGroups().contains(group1));
        groupDao.promoteToModerator(group1, account1);
        assertFalse(account1.getSubscriptions().contains(group1));
        assertFalse(account1.getModeratedGroups().contains(group1));
        assertTrue(account1.getRequestedGroups().contains(group1));
    }

    @Test
    public void acceptRequestFromRequesterChangeGroupSide() {
        assertTrue(group1.getRequesters().contains(account1));
        assertFalse(group1.getSubscribers().contains(account1));
        assertFalse(group1.getModerators().contains(account1));
        groupDao.acceptRequest(group1, account1);
        Group group = groupDao.get(1);
        assertFalse(group.getRequesters().contains(account1));
        assertTrue(group.getSubscribers().contains(account1));
        assertFalse(group.getModerators().contains(account1));
    }

    @Test
    public void acceptRequestFromRequesterChangeGroupArgumentInstance() {
        assertTrue(group1.getRequesters().contains(account1));
        assertFalse(group1.getSubscribers().contains(account1));
        assertFalse(group1.getModerators().contains(account1));
        groupDao.acceptRequest(group1, account1);
        assertFalse(group1.getRequesters().contains(account1));
        assertTrue(group1.getSubscribers().contains(account1));
        assertFalse(group1.getModerators().contains(account1));
    }

    @Test
    public void acceptRequestFromRequesterChangeAccountSide() {
        assertTrue(account1.getRequestedGroups().contains(group1));
        assertFalse(account1.getSubscriptions().contains(group1));
        assertFalse(account1.getModeratedGroups().contains(group1));
        groupDao.acceptRequest(group1, account1);
        Account account = accountDao.get(1);
        assertFalse(account.getRequestedGroups().contains(group1));
        assertTrue(account.getSubscriptions().contains(group1));
        assertFalse(account.getModeratedGroups().contains(group1));
    }

    @Test
    public void acceptRequestFromRequesterChangeAccountArgumentInstance() {
        assertTrue(account1.getRequestedGroups().contains(group1));
        assertFalse(account1.getSubscriptions().contains(group1));
        assertFalse(account1.getModeratedGroups().contains(group1));
        groupDao.acceptRequest(group1, account1);
        assertFalse(account1.getRequestedGroups().contains(group1));
        assertTrue(account1.getSubscriptions().contains(group1));
        assertFalse(account1.getModeratedGroups().contains(group1));
    }

    @Test
    public void acceptRequestFromSubscriberNotChangeGroupSide() {
        assertFalse(group1.getRequesters().contains(account3));
        assertTrue(group1.getSubscribers().contains(account3));
        assertFalse(group1.getModerators().contains(account3));
        groupDao.acceptRequest(group1, account3);
        Group group = groupDao.get(1);
        assertFalse(group.getRequesters().contains(account3));
        assertTrue(group.getSubscribers().contains(account3));
        assertFalse(group.getModerators().contains(account3));
    }

    @Test
    public void acceptRequestFromSubscriberNotChangeGroupArgumentInstance() {
        assertFalse(group1.getRequesters().contains(account3));
        assertTrue(group1.getSubscribers().contains(account3));
        assertFalse(group1.getModerators().contains(account3));
        groupDao.acceptRequest(group1, account3);
        assertFalse(group1.getRequesters().contains(account3));
        assertTrue(group1.getSubscribers().contains(account3));
        assertFalse(group1.getModerators().contains(account3));
    }

    @Test
    public void acceptRequestFromSubscriberNotChangeAccountSide() {
        assertFalse(account3.getRequestedGroups().contains(group1));
        assertTrue(account3.getSubscriptions().contains(group1));
        assertFalse(account3.getModeratedGroups().contains(group1));
        groupDao.acceptRequest(group1, account3);
        Account account = accountDao.get(3);
        assertFalse(account.getRequestedGroups().contains(group1));
        assertTrue(account.getSubscriptions().contains(group1));
        assertFalse(account.getModeratedGroups().contains(group1));
    }

    @Test
    public void acceptRequestFromSubscriberNotChangeAccountArgumentInstance() {
        assertFalse(account3.getRequestedGroups().contains(group1));
        assertTrue(account3.getSubscriptions().contains(group1));
        assertFalse(account3.getModeratedGroups().contains(group1));
        groupDao.acceptRequest(group1, account3);
        assertFalse(account3.getRequestedGroups().contains(group1));
        assertTrue(account3.getSubscriptions().contains(group1));
        assertFalse(account3.getModeratedGroups().contains(group1));
    }

    @Test
    public void acceptRequestFromModeratorNotChangeGroupSide() {
        assertFalse(group2.getRequesters().contains(account3));
        assertFalse(group2.getSubscribers().contains(account3));
        assertTrue(group2.getModerators().contains(account3));
        groupDao.acceptRequest(group2, account3);
        Group group = groupDao.get(2);
        assertFalse(group.getRequesters().contains(account3));
        assertFalse(group.getSubscribers().contains(account3));
        assertTrue(group.getModerators().contains(account3));
    }

    @Test
    public void acceptRequestFromModeratorNotChangeGroupArgumentInstance() {
        assertFalse(group2.getRequesters().contains(account3));
        assertFalse(group2.getSubscribers().contains(account3));
        assertTrue(group2.getModerators().contains(account3));
        groupDao.acceptRequest(group2, account3);
        assertFalse(group2.getRequesters().contains(account3));
        assertFalse(group2.getSubscribers().contains(account3));
        assertTrue(group2.getModerators().contains(account3));
    }

    @Test
    public void acceptRequestFromModeratorNotChangeAccountSide() {
        assertFalse(account3.getRequestedGroups().contains(group2));
        assertFalse(account3.getSubscriptions().contains(group2));
        assertTrue(account3.getModeratedGroups().contains(group2));
        groupDao.acceptRequest(group2, account3);
        Account account = accountDao.get(3);
        assertFalse(account.getRequestedGroups().contains(group2));
        assertFalse(account.getSubscriptions().contains(group2));
        assertTrue(account.getModeratedGroups().contains(group2));
    }

    @Test
    public void acceptRequestFromModeratorNotChangeAccountArgumentInstance() {
        assertFalse(account3.getRequestedGroups().contains(group2));
        assertFalse(account3.getSubscriptions().contains(group2));
        assertTrue(account3.getModeratedGroups().contains(group2));
        groupDao.acceptRequest(group2, account3);
        assertFalse(account3.getRequestedGroups().contains(group2));
        assertFalse(account3.getSubscriptions().contains(group2));
        assertTrue(account3.getModeratedGroups().contains(group2));
    }

    @Test
    public void declineRequestFromRequesterChangeGroupSide() {
        assertTrue(group1.getRequesters().contains(account1));
        assertFalse(group1.getSubscribers().contains(account1));
        assertFalse(group1.getModerators().contains(account1));
        groupDao.declineRequest(group1, account1);
        Group group = groupDao.get(1);
        assertFalse(group.getRequesters().contains(account1));
        assertFalse(group.getSubscribers().contains(account1));
        assertFalse(group.getModerators().contains(account1));
    }

    @Test
    public void declineRequestFromRequesterChangeGroupArgumentInstance() {
        assertTrue(group1.getRequesters().contains(account1));
        assertFalse(group1.getSubscribers().contains(account1));
        assertFalse(group1.getModerators().contains(account1));
        groupDao.declineRequest(group1, account1);
        assertFalse(group1.getRequesters().contains(account1));
        assertFalse(group1.getSubscribers().contains(account1));
        assertFalse(group1.getModerators().contains(account1));
    }

    @Test
    public void declineRequestFromRequesterChangeAccountSide() {
        assertTrue(account1.getRequestedGroups().contains(group1));
        assertFalse(account1.getSubscriptions().contains(group1));
        assertFalse(account1.getModeratedGroups().contains(group1));
        groupDao.declineRequest(group1, account1);
        Account account = accountDao.get(1);
        assertFalse(account.getRequestedGroups().contains(group1));
        assertFalse(account.getSubscriptions().contains(group1));
        assertFalse(account.getModeratedGroups().contains(group1));
    }

    @Test
    public void declineRequestFromRequesterChangeAccountArgumentInstance() {
        assertTrue(account1.getRequestedGroups().contains(group1));
        assertFalse(account1.getSubscriptions().contains(group1));
        assertFalse(account1.getModeratedGroups().contains(group1));
        groupDao.declineRequest(group1, account1);
        assertFalse(account1.getRequestedGroups().contains(group1));
        assertFalse(account1.getSubscriptions().contains(group1));
        assertFalse(account1.getModeratedGroups().contains(group1));
    }

    @Test
    public void declineRequestFromSubscriberNotChangeGroupSide() {
        assertFalse(group1.getRequesters().contains(account3));
        assertTrue(group1.getSubscribers().contains(account3));
        assertFalse(group1.getModerators().contains(account3));
        groupDao.declineRequest(group1, account3);
        Group group = groupDao.get(1);
        assertFalse(group.getRequesters().contains(account3));
        assertTrue(group.getSubscribers().contains(account3));
        assertFalse(group.getModerators().contains(account3));
    }

    @Test
    public void declineRequestFromSubscriberNotChangeGroupArgumentInstance() {
        assertFalse(group1.getRequesters().contains(account3));
        assertTrue(group1.getSubscribers().contains(account3));
        assertFalse(group1.getModerators().contains(account3));
        groupDao.declineRequest(group1, account3);
        assertFalse(group1.getRequesters().contains(account3));
        assertTrue(group1.getSubscribers().contains(account3));
        assertFalse(group1.getModerators().contains(account3));
    }

    @Test
    public void declineRequestFromSubscriberNotChangeAccountSide() {
        assertFalse(account3.getRequestedGroups().contains(group1));
        assertTrue(account3.getSubscriptions().contains(group1));
        assertFalse(account3.getModeratedGroups().contains(group1));
        groupDao.declineRequest(group1, account3);
        Account account = accountDao.get(3);
        assertFalse(account.getRequestedGroups().contains(group1));
        assertTrue(account.getSubscriptions().contains(group1));
        assertFalse(account.getModeratedGroups().contains(group1));
    }

    @Test
    public void declineRequestFromSubscriberNotChangeAccountArgumentInstance() {
        assertFalse(account3.getRequestedGroups().contains(group1));
        assertTrue(account3.getSubscriptions().contains(group1));
        assertFalse(account3.getModeratedGroups().contains(group1));
        groupDao.declineRequest(group1, account3);
        assertFalse(account3.getRequestedGroups().contains(group1));
        assertTrue(account3.getSubscriptions().contains(group1));
        assertFalse(account3.getModeratedGroups().contains(group1));
    }

    @Test
    public void declineRequestFromModeratorNotChangeGroupSide() {
        assertFalse(group2.getRequesters().contains(account3));
        assertFalse(group2.getSubscribers().contains(account3));
        assertTrue(group2.getModerators().contains(account3));
        groupDao.declineRequest(group2, account3);
        Group group = groupDao.get(2);
        assertFalse(group.getRequesters().contains(account3));
        assertFalse(group.getSubscribers().contains(account3));
        assertTrue(group.getModerators().contains(account3));
    }

    @Test
    public void declineRequestFromModeratorNotChangeGroupArgumentInstance() {
        assertFalse(group2.getRequesters().contains(account3));
        assertFalse(group2.getSubscribers().contains(account3));
        assertTrue(group2.getModerators().contains(account3));
        groupDao.declineRequest(group2, account3);
        assertFalse(group2.getRequesters().contains(account3));
        assertFalse(group2.getSubscribers().contains(account3));
        assertTrue(group2.getModerators().contains(account3));
    }

    @Test
    public void declineRequestFromModeratorNotChangeAccountSide() {
        assertFalse(account3.getRequestedGroups().contains(group2));
        assertFalse(account3.getSubscriptions().contains(group2));
        assertTrue(account3.getModeratedGroups().contains(group2));
        groupDao.declineRequest(group2, account3);
        Account account = accountDao.get(3);
        assertFalse(account.getRequestedGroups().contains(group2));
        assertFalse(account.getSubscriptions().contains(group2));
        assertTrue(account.getModeratedGroups().contains(group2));
    }

    @Test
    public void declineRequestFromModeratorNotChangeAccountArgumentInstance() {
        assertFalse(account3.getRequestedGroups().contains(group2));
        assertFalse(account3.getSubscriptions().contains(group2));
        assertTrue(account3.getModeratedGroups().contains(group2));
        groupDao.declineRequest(group2, account3);
        assertFalse(account3.getRequestedGroups().contains(group2));
        assertFalse(account3.getSubscriptions().contains(group2));
        assertTrue(account3.getModeratedGroups().contains(group2));
    }

    private void init() {
        account1 = new Account(1, "Ivanov", "Ivan", null, null, null, null, "ivanov@gmail.com", null, null, null, "111", null);
        account2 = new Account(2, "Petrov", "Petr", "Petrovich", parse("2000-01-01"),
                "Russia, Moscow, Red Square", "Russia, Moscow, Kremlin", "petrov@gmail.com", "petrov_icq", "petrov_skype",
                "petrov_additional_info", "123", null);
        account3 = new Account(3, "Petrov", "Petr", "Petrovich", parse("2000-01-01"),
                "Russia, Moscow, Red Square", "Russia, Moscow, Kremlin", "petrov_copy@gmail.com", "petrov_icq", "petrov_skype",
                "Copy of Petrov with another e-mail", "123", null);
        account4 = new Account(4, "Попова", "Ольга", "Викторовна", parse("1990-12-20"),
                "Россия, г. Омск, ул. Центральная, д. 10-12", "Домохозяйка", "popova@yandex.ru", "popova_icq",
                "popova_skype", "Пользователь, записанный кириллицей", "12345", null);

        group1 = new Group(1, account4, "Empty group", null);
        group2 = new Group(2, account4, "Java", "Java programming language");
        group3 = new Group(3, account4, "Космос", "Космос в картинках и фактах");

        Phone phone1 = new Phone(1, account3, "11111111111", HOME);
        Phone phone2 = new Phone(2, account3, "22222222222", WORK);
        Phone phone3 = new Phone(3, account4, "44444444444", HOME);
        Phone phone4 = new Phone(4, account4, "55555555555", HOME);
        Phone phone5 = new Phone(5, account4, "66666666666", WORK);
        Phone phone6 = new Phone(6, account4, "22222222222", WORK);

        account3.setPersonalPhones(new ArrayList<>(singletonList(phone1)));
        account3.setWorkPhones(new ArrayList<>(singletonList(phone2)));
        account4.setPersonalPhones(new ArrayList<>(asList(phone3, phone4)));
        account4.setWorkPhones(new ArrayList<>(asList(phone5, phone6)));

        account2.setFriends(new HashSet<>(singletonList(account4)));
        account3.setFriends(new HashSet<>(singletonList(account4)));
        account4.setFriends(new HashSet<>(asList(account2, account3)));
        account1.setOutcomingFriendRequests(new HashSet<>(asList(account2, account3)));
        account2.setOutcomingFriendRequests(new HashSet<>(singletonList(account3)));
        account2.setIncomingFriendRequests(new HashSet<>(singletonList(account1)));
        account3.setIncomingFriendRequests(new HashSet<>(asList(account1, account2)));

        account4.setCreatedGroups(new ArrayList<>(asList(group1, group2, group3)));
        account2.setModeratedGroups(new HashSet<>(singletonList(group3)));
        account3.setModeratedGroups(new HashSet<>(asList(group2, group3)));
        account1.setSubscriptions(new HashSet<>(asList(group2, group3)));
        account2.setSubscriptions(new HashSet<>(singletonList(group2)));
        account3.setSubscriptions(new HashSet<>(singletonList(group1)));
        account1.setRequestedGroups(new HashSet<>(singletonList(group1)));
        account2.setRequestedGroups(new HashSet<>(singletonList(group1)));

        group1.setModerators(new HashSet<>());
        group1.setSubscribers(new HashSet<>(singletonList(account3)));
        group1.setRequesters(new HashSet<>(asList(account1, account2)));
        group2.setModerators(new HashSet<>(singletonList(account3)));
        group2.setSubscribers(new HashSet<>(asList(account1, account2)));
        group2.setRequesters(new HashSet<>());
        group3.setModerators(new HashSet<>(asList(account2, account3)));
        group3.setSubscribers(new HashSet<>(singletonList(account1)));
        group3.setRequesters(new HashSet<>());
    }
}