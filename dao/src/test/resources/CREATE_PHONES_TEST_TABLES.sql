CREATE TABLE IF NOT EXISTS phone_types (
  type CHAR(4) PRIMARY KEY
);

CREATE TABLE IF NOT EXISTS phones
(
  phone_id    BIGINT AUTO_INCREMENT,
  account_id  BIGINT      NOT NULL,
  phoneNumber VARCHAR(30) NOT NULL,
  type        CHAR(4)     NOT NULL,
  CONSTRAINT phone_pk PRIMARY KEY (phone_id),
  CONSTRAINT accountId_fk FOREIGN KEY (account_id) REFERENCES accounts (user_id)
    ON DELETE CASCADE,
  CONSTRAINT phonetype_fk FOREIGN KEY (type) REFERENCES phone_types (type)
    ON DELETE CASCADE
);