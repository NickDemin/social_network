CREATE TABLE IF NOT EXISTS friends
(
  first_account_id  BIGINT NOT NULL,
  second_account_id BIGINT NOT NULL,

  CONSTRAINT relations_pk PRIMARY KEY (first_account_id, second_account_id),
  CONSTRAINT first_account_fk FOREIGN KEY (first_account_id) REFERENCES accounts (user_id)
    ON DELETE CASCADE,
  CONSTRAINT second_account_fk FOREIGN KEY (second_account_id) REFERENCES accounts (user_id)
    ON DELETE CASCADE

);

CREATE TABLE IF NOT EXISTS requests_to_friends
(
  account_from_id BIGINT NOT NULL,
  account_to_id   BIGINT NOT NULL,

  CONSTRAINT friend_request_pk PRIMARY KEY (account_from_id, account_to_id),
  CONSTRAINT from_account_fk FOREIGN KEY (account_from_id) REFERENCES accounts (user_id)
    ON DELETE CASCADE,
  CONSTRAINT to_account_fk FOREIGN KEY (account_to_id) REFERENCES accounts (user_id)
    ON DELETE CASCADE

);