package com.getjavajob.training.web1701.dyominn.dao;

import com.getjavajob.training.web1701.dyominn.common.models.Account;
import com.getjavajob.training.web1701.dyominn.common.models.Group;
import com.getjavajob.training.web1701.dyominn.common.models.Phone;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import javax.persistence.TypedQuery;
import javax.persistence.criteria.*;
import java.util.List;
import java.util.Set;

import static com.getjavajob.training.web1701.dyominn.common.utils.CryptPassword.check;

@Repository
public class AccountDao extends AbstractDao<Account> {
    private static final Logger logger = LoggerFactory.getLogger(AccountDao.class);

    public AccountDao() {
    }

    @Override
    public Account get(long id) {
        logger.info("Started method: get account by id with initialized lazy collections");
        Account account = super.get(id);
        if (account != null) {
            logger.debug("Read all lazy collections");
            readLazyPhones(account);
            readLazyFriendshipRelations(account);
            readLazyGroups(account);
        }
        logger.info("Completed method: get account by id with initialized lazy collections");
        return account;
    }

    public Account getAccountForAuthorisation(String email, String password, boolean passwordAlreadyHashed) {
        logger.info("Started method: get account by email and password/hash");
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Account> criteriaQuery = criteriaBuilder.createQuery(Account.class);
        Root<Account> root = criteriaQuery.from(Account.class);
        criteriaQuery.select(root);
        criteriaQuery.where(criteriaBuilder.equal(root.get("email"), email));
        List<Account> accounts = entityManager.createQuery(criteriaQuery).getResultList();
        if (accounts.isEmpty()) {
            logger.info("Completed method: get account by email and password/hash. No accounts founded");
            return null;
        }
        if (accounts.size() > 1) {
            logger.error("Accounts' email in DB must be unique. But with {} founded: {} rows", email, accounts.size());
            return null;
        }
        Account account = accounts.get(0);
        if (logger.isDebugEnabled()) {
            logger.debug("Account selected: {}", account);
        }
        readLazyPhones(account);
        readLazyFriendshipRelations(account);
        logger.debug("Account's lazy collections for phones and friendship relations were read");

        String hash = account.getPassword();
        if (passwordAlreadyHashed) {
            logger.info("Completed method: get account by email and hash");
            return hash.equals(password) ? account : null;
        } else {
            logger.info("Completed method: get account by email and password");
            return check(password, hash) ? account : null;
        }
    }

    public List<Account> getAccountsByNameOrSurnamePattern(String searchQuery) {
        logger.info("Started method: get list of all accounts with name/surname pattern");
        String pattern = searchQuery + "%";

        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Account> criteriaQuery = criteriaBuilder.createQuery(Account.class);
        Root<Account> root = criteriaQuery.from(Account.class);
        criteriaQuery.select(root);

        Expression<String> firstLastName = criteriaBuilder.concat(root.get("firstName"), " ");
        firstLastName = criteriaBuilder.concat(firstLastName, root.get("lastName"));
        Expression<String> lastFirstName = criteriaBuilder.concat(root.get("lastName"), " ");
        lastFirstName = criteriaBuilder.concat(lastFirstName, root.get("firstName"));
        Predicate searchClause = criteriaBuilder.or(criteriaBuilder.like(firstLastName, pattern),
                criteriaBuilder.like(lastFirstName, pattern));
        criteriaQuery.where(searchClause);

        List<Account> accounts = entityManager.createQuery(criteriaQuery).getResultList();
        if (logger.isDebugEnabled()) {
            logger.debug("With pattern {} founded {} accounts", searchQuery, accounts.size());
        }
        logger.info("Completed method: get list of all accounts with name/surname pattern");
        return accounts;
    }

    public List<Account> getAccountsByNameOrSurnamePattern(String searchQuery, int startIndex, int accountsQuantity) {
        logger.info("Started method: get fixed list of accounts with name/surname pattern");
        String pattern = searchQuery + "%";

        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Account> criteriaQuery = criteriaBuilder.createQuery(Account.class);
        Root<Account> root = criteriaQuery.from(Account.class);
        criteriaQuery.select(root);

        Expression<String> firstLastName = criteriaBuilder.concat(root.get("firstName"), " ");
        firstLastName = criteriaBuilder.concat(firstLastName, root.get("lastName"));
        Expression<String> lastFirstName = criteriaBuilder.concat(root.get("lastName"), " ");
        lastFirstName = criteriaBuilder.concat(lastFirstName, root.get("firstName"));
        Predicate searchClause = criteriaBuilder.or(criteriaBuilder.like(firstLastName, pattern),
                criteriaBuilder.like(lastFirstName, pattern));
        criteriaQuery.where(searchClause);

        TypedQuery<Account> accountsWithOffset = entityManager.createQuery(criteriaQuery).setFirstResult(startIndex);
        List<Account> accounts = accountsWithOffset.setMaxResults(accountsQuantity).getResultList();
        if (logger.isDebugEnabled()) {
            logger.debug("With pattern {} founded {} accounts. Offset(start index): {}, quantity: {}", searchQuery,
                    accounts.size(), startIndex, accountsQuantity);
        }
        logger.info("Completed method: get fixed list of accounts with name/surname pattern");
        return accounts;
    }

    public long countAccountsWithNameOrSurnamePattern(String searchQuery) {
        logger.info("Started method: count accounts with name/surname pattern");
        String pattern = searchQuery + "%";

        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Long> criteriaQuery = criteriaBuilder.createQuery(Long.class);
        Root<Account> root = criteriaQuery.from(Account.class);
        criteriaQuery.select(criteriaBuilder.count(root));

        Expression<String> firstLastName = criteriaBuilder.concat(root.get("firstName"), " ");
        firstLastName = criteriaBuilder.concat(firstLastName, root.get("lastName"));
        Expression<String> lastFirstName = criteriaBuilder.concat(root.get("lastName"), " ");
        lastFirstName = criteriaBuilder.concat(lastFirstName, root.get("firstName"));
        Predicate searchClause = criteriaBuilder.or(criteriaBuilder.like(firstLastName, pattern),
                criteriaBuilder.like(lastFirstName, pattern));
        criteriaQuery.where(searchClause);

        long accounts = entityManager.createQuery(criteriaQuery).getSingleResult();
        if (logger.isDebugEnabled()) {
            logger.debug("With pattern {} founded {} accounts", searchQuery, accounts);
        }
        logger.info("Completed method: count accounts with name/surname pattern");
        return accounts;
    }

    // Friends relationship logic

    public void removeFromFriends(long initiatorFriendId, long removableFriendId, Account sessionAccountForUpdate) {
        logger.info("Started method: remove account from friends");
        Account initiatorFriend = get(initiatorFriendId);
        Account removableFriend = get(removableFriendId);
        boolean wereFriends = initiatorFriend.getFriends().remove(removableFriend);
        if (wereFriends) {
            removableFriend.getFriends().remove(initiatorFriend);
            initiatorFriend.getIncomingFriendRequests().add(removableFriend);
            entityManager.merge(initiatorFriend);
            entityManager.merge(removableFriend);
            updateStateOfFriendshipRelations(initiatorFriend, sessionAccountForUpdate);
            logger.debug("Accounts' friendship relations successfully changed");
        }
        logger.info("Completed method: remove account from friends");
    }

    public void acceptFriendshipRequest(long receiverId, long requesterId, Account sessionAccountForUpdate) {
        logger.info("Started method: accept friendship request");
        Account receiver = get(receiverId);
        Account requester = get(requesterId);
        boolean haveRequest = receiver.getIncomingFriendRequests().remove(requester);
        if (haveRequest) {
            requester.getOutcomingFriendRequests().remove(receiver);
            receiver.getFriends().add(requester);
            requester.getFriends().add(receiver);
            entityManager.merge(receiver);
            entityManager.merge(requester);
            updateStateOfFriendshipRelations(receiver, sessionAccountForUpdate);
            logger.debug("Friendship request successfully accepted");
        }
        logger.info("Completed method: accept friendship request");
    }

    public void declineFriendshipRequest(long receiverId, long requesterId, Account sessionAccountForUpdate) {
        logger.info("Started method: decline friendship request");
        Account receiver = get(receiverId);
        Account requester = get(requesterId);
        boolean haveIncomingRequest = receiver.getIncomingFriendRequests().remove(requester);
        if (haveIncomingRequest) {
            requester.getOutcomingFriendRequests().remove(receiver);
            entityManager.merge(receiver);
            entityManager.merge(requester);
            updateStateOfFriendshipRelations(receiver, sessionAccountForUpdate);
            logger.debug("Friendship request successfully declined");
        }
        logger.info("Completed method: decline friendship request");
    }

    public void cancelOutgoingRequest(long requesterId, long receiverId, Account sessionAccountForUpdate) {
        logger.info("Started method: cancel outgoing friendship request");
        Account requester = get(requesterId);
        Account receiver = get(receiverId);
        boolean haveOutcomingRequest = requester.getOutcomingFriendRequests().remove(receiver);
        if (haveOutcomingRequest) {
            receiver.getIncomingFriendRequests().remove(requester);
            entityManager.merge(receiver);
            entityManager.merge(requester);
            updateStateOfFriendshipRelations(requester, sessionAccountForUpdate);
            logger.debug("Friendship request successfully canceled");
        }
        logger.info("Completed method: cancel outgoing friendship request");
    }

    public void sendFriendshipRequest(long requesterId, long receiverId, Account sessionAccountForUpdate) {
        logger.info("Started method: send outgoing friendship request");
        if (receiverId == requesterId) {
            logger.debug("Made an attempt to send friendship request to yourself");
            logger.info("Completed method: send outgoing friendship request. ");
            return;
        }
        Account requester = get(requesterId);
        Account receiver = get(receiverId);
        if (requester.getOutcomingFriendRequests().contains(receiver)) {
            logger.debug("Friendship request already made");
            logger.info("Completed method: send outgoing friendship request. ");
            return;
        }
        boolean haveCounterRequest = requester.getIncomingFriendRequests().contains(receiver);
        if (haveCounterRequest) {
            logger.debug("Counter-request already made. Make accounts as friends");
            requester.getIncomingFriendRequests().remove(receiver);
            receiver.getOutcomingFriendRequests().remove(requester);
            receiver.getFriends().add(requester);
            requester.getFriends().add(receiver);
        } else {
            requester.getOutcomingFriendRequests().add(receiver);
            receiver.getIncomingFriendRequests().add(requester);
        }
        entityManager.merge(receiver);
        entityManager.merge(requester);
        updateStateOfFriendshipRelations(requester, sessionAccountForUpdate);
        logger.info("Completed method: send outgoing friendship request");
    }

    // Accounts' groups logic

    public void sendGroupRequest(Account requester, Group group) {
        logger.info("Started method: send request to group");
        Account managedRequester = get(requester.getId());
        if (managedRequester.getRequestedGroups().contains(group)) {
            logger.debug("Request to group already exist");
            logger.info("Completed method: send request to group");
            return;
        }
        if (!isGroupMember(requester, group)) {
            logger.debug("Requester is not member of group. Send request");
            managedRequester.getRequestedGroups().add(group);
            group.getRequesters().add(managedRequester);
            entityManager.merge(managedRequester);
            entityManager.merge(group);
            updateStateOfGroups(managedRequester, requester);
        }
        logger.info("Completed method: send request to group");
    }

    public void cancelRequestToGroup(Account requester, Group group) {
        logger.info("Started method: cancel request to group");
        Account managedRequester = get(requester.getId());
        boolean haveRequest = managedRequester.getRequestedGroups().remove(group);
        if (haveRequest) {
            logger.debug("Cancel the existing request");
            group.getRequesters().remove(managedRequester);
            entityManager.merge(managedRequester);
            entityManager.merge(group);
            updateStateOfGroups(managedRequester, requester);
        }
        logger.info("Completed method: cancel request to group");
    }

    public void leaveGroup(Account member, Group group) {
        logger.info("Started method: leave group");
        Account managerMember = get(member.getId());
        boolean isSubscriber = managerMember.getSubscriptions().remove(group);
        boolean isModerator = managerMember.getModeratedGroups().remove(group);
        if (isSubscriber || isModerator) {
            logger.debug("Account is subscriber/moderator. Leave the group");
            group.getSubscribers().remove(managerMember);
            group.getModerators().remove(managerMember);
            entityManager.merge(managerMember);
            entityManager.merge(group);
            updateStateOfGroups(managerMember, member);
        }
        logger.info("Completed method: leave group");
    }

    private boolean isGroupMember(Account account, Group group) {
        return account.getCreatedGroups().contains(group) ||
                account.getModeratedGroups().contains(group) ||
                account.getSubscriptions().contains(group);
    }

    // JPA Lazy loading

    private void readLazyPhones(Account account) {
        logger.info("Started method: read lazy phones collections");
        List<Phone> personalPhones = account.getPersonalPhones();
        if (personalPhones != null) {
            personalPhones.size();
        }
        List<Phone> workPhones = account.getWorkPhones();
        if (workPhones != null) {
            workPhones.size();
        }
        logger.info("Completed method: read lazy phones collections");
    }

    private void readLazyFriendshipRelations(Account account) {
        logger.info("Started method: read lazy friendship relations collections");
        Set<Account> friends = account.getFriends();
        if (friends != null) {
            friends.size();
        }
        Set<Account> incomingRequests = account.getIncomingFriendRequests();
        if (incomingRequests != null) {
            incomingRequests.size();
        }
        Set<Account> outgoingRequests = account.getOutcomingFriendRequests();
        if (outgoingRequests != null) {
            outgoingRequests.size();
        }
        logger.info("Completed method: read lazy friendship relations collections");
    }

    private void readLazyGroups(Account account) {
        logger.info("Started method: read lazy groups collections");
        List<Group> createdGroups = account.getCreatedGroups();
        if (createdGroups != null) {
            createdGroups.size();
        }
        Set<Group> moderatedGroups = account.getModeratedGroups();
        if (moderatedGroups != null) {
            moderatedGroups.size();
        }
        Set<Group> subscriptions = account.getSubscriptions();
        if (subscriptions != null) {
            subscriptions.size();
        }
        Set<Group> requests = account.getRequestedGroups();
        if (requests != null) {
            requests.size();
        }
        logger.info("Completed method: read lazy groups collections");
    }

    // Copy state of persistent entities to detached argument instances

    private void updateStateOfFriendshipRelations(Account source, Account destination) {
        logger.info("Started method: copy state of account's friendship relations");
        destination.setFriends(source.getFriends());
        destination.setIncomingFriendRequests(source.getIncomingFriendRequests());
        destination.setOutcomingFriendRequests(source.getOutcomingFriendRequests());
        logger.info("Completed method: copy state of account's friendship relations");
    }

    private void updateStateOfGroups(Account source, Account destination) {
        logger.info("Started method: copy state of account's groups collections");
        destination.setCreatedGroups(source.getCreatedGroups());
        destination.setModeratedGroups(source.getModeratedGroups());
        destination.setSubscriptions(source.getSubscriptions());
        destination.setRequestedGroups(source.getRequestedGroups());
        logger.info("Completed method: copy state of account's groups collections");
    }
}