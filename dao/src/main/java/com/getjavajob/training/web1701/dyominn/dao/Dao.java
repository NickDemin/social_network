package com.getjavajob.training.web1701.dyominn.dao;

import com.getjavajob.training.web1701.dyominn.common.Entity;

import java.util.List;

public interface Dao<E extends Entity> {

    E get(long id);

    List<E> getAll();

    E insert(E entity);

    void update(E entity);

    void delete(long id);
}