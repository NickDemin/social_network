package com.getjavajob.training.web1701.dyominn.dao;

import com.getjavajob.training.web1701.dyominn.common.Entity;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.lang.reflect.ParameterizedType;
import java.util.List;

public abstract class AbstractDao<E extends Entity> implements Dao<E> {
    private static final Logger logger = LoggerFactory.getLogger(AbstractDao.class);
    @PersistenceContext
    protected EntityManager entityManager;
    private final Class<E> entityClass;

    @SuppressWarnings("unchecked") // ok, generic class instance getter
    public AbstractDao() {
        ParameterizedType genericSuperclass = (ParameterizedType) getClass().getGenericSuperclass();
        this.entityClass = (Class<E>) genericSuperclass.getActualTypeArguments()[0];
    }

    @Override
    public E get(long id) {
        logger.info("Started method: get entity by id");
        logger.debug("Entity id: {}", id);
        E entity = entityManager.find(entityClass, id);
        if (logger.isDebugEnabled()) {
            logger.debug("Entity selected: {}", entity);
        }
        logger.info("Completed method: get entity by id");
        return entity;
    }

    @Override
    public List<E> getAll() {
        logger.info("Started method: get list of all entities in database");
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<E> criteriaQuery = criteriaBuilder.createQuery(entityClass);
        Root<E> root = criteriaQuery.from(entityClass);
        criteriaQuery.select(root);
        List<E> entities = entityManager.createQuery(criteriaQuery).getResultList();
        if (logger.isDebugEnabled()) {
            logger.debug("Selected {} {} entities", entities.size(), this.entityClass.getSimpleName());
        }
        logger.info("Completed method: get list of all entities in database");
        return entities;
    }

    @Override
    public E insert(E entity) {
        logger.info("Started method: insert new entity");
        if (logger.isDebugEnabled()) {
            logger.debug("Entity to insert: {}", entity);
        }
        entityManager.persist(entity);
        if (logger.isDebugEnabled()) {
            logger.debug("Entity inserted: {}", entity);
        }
        logger.info("Completed method: insert new entity");
        return entity;
    }

    @Override
    public void update(E entity) {
        logger.info("Started method: update entity");
        entityManager.merge(entity);
        logger.info("Completed method: update entity");
    }

    @Override
    public void delete(long id) {
        logger.info("Started method: delete entity by id");
        E entity = entityManager.find(entityClass, id);
        if (logger.isDebugEnabled()) {
            logger.debug("Entity instance to delete: {}", entity);
        }
        entityManager.remove(entity);
        logger.info("Completed method: delete entity by id");
    }
}