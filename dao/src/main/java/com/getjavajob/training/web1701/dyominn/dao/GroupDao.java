package com.getjavajob.training.web1701.dyominn.dao;

import com.getjavajob.training.web1701.dyominn.common.models.Account;
import com.getjavajob.training.web1701.dyominn.common.models.Group;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import java.util.Set;

@Repository
public class GroupDao extends AbstractDao<Group> {
    private static final Logger logger = LoggerFactory.getLogger(GroupDao.class);

    public GroupDao() {
    }

    @Override
    public Group get(long id) {
        logger.info("Started method: get group by id with initialized lazy collections");
        Group group = super.get(id);
        if (group != null) {
            logger.debug("Read all lazy collections");
            readLazyMembers(group);
        }
        logger.info("Completed method: get group by id with initialized lazy collections");
        return group;
    }

    public void removeGroupMember(Group group, Account member) {
        logger.info("Started method: delete group's member");
        Group managedGroup = get(group.getId());
        boolean isSubscriber = managedGroup.getSubscribers().remove(member);
        boolean isModerator = managedGroup.getModerators().remove(member);
        if (isModerator || isSubscriber) {
            logger.debug("Account is member of the group. Remove account's membership");
            member.getSubscriptions().remove(group);
            member.getModeratedGroups().remove(group);
            entityManager.merge(managedGroup);
            entityManager.merge(member);
            updateStateOfMembership(managedGroup, group);
        }
        logger.info("Completed method: delete group's member");
    }

    public void dismissFromModerator(Group group, Account member) {
        logger.info("Started method: dismiss moderator");
        Group managedGroup = get(group.getId());
        boolean isModerator = managedGroup.getModerators().remove(member);
        if (isModerator) {
            logger.debug("Account is moderator of the group. Transfer to subscribers");
            managedGroup.getSubscribers().add(member);
            member.getModeratedGroups().remove(managedGroup);
            member.getSubscriptions().add(managedGroup);
            entityManager.merge(managedGroup);
            entityManager.merge(member);
            updateStateOfMembership(managedGroup, group);
        }
        logger.info("Completed method: dismiss moderator");
    }

    public void promoteToModerator(Group group, Account member) {
        logger.info("Started method: promote subscriber to moderator");
        Group managedGroup = get(group.getId());
        boolean isSubscriber = managedGroup.getSubscribers().remove(member);
        if (isSubscriber) {
            logger.debug("Account is subscriber of the group. Promote to moderators");
            managedGroup.getModerators().add(member);
            member.getSubscriptions().remove(managedGroup);
            member.getModeratedGroups().add(managedGroup);
            entityManager.merge(managedGroup);
            entityManager.merge(member);
            updateStateOfMembership(managedGroup, group);
        }
        logger.info("Completed method: promote subscriber to moderator");
    }

    public void acceptRequest(Group group, Account member) {
        logger.info("Started method: accept request to group");
        Group managedGroup = get(group.getId());
        boolean isRequester = managedGroup.getRequesters().remove(member);
        if (isRequester) {
            logger.debug("Account is requester of the group. Transfer to subscribers");
            managedGroup.getSubscribers().add(member);
            member.getRequestedGroups().remove(managedGroup);
            member.getSubscriptions().add(managedGroup);
            entityManager.merge(managedGroup);
            entityManager.merge(member);
            updateStateOfMembership(managedGroup, group);
        }
        logger.info("Completed method: accept request to group");
    }

    public void declineRequest(Group group, Account member) {
        logger.info("Started method: decline request to group");
        Group managedGroup = get(group.getId());
        boolean isRequester = managedGroup.getRequesters().remove(member);
        if (isRequester) {
            logger.debug("Account is requester of the group. Decline request");
            member.getRequestedGroups().remove(managedGroup);
            entityManager.merge(managedGroup);
            entityManager.merge(member);
            updateStateOfMembership(managedGroup, group);
        }
        logger.info("Completed method: decline request to group");
    }

    private void readLazyMembers(Group group) {
        logger.info("Started method: read lazy membership collections");
        Set<Account> moderators = group.getModerators();
        if (moderators != null) {
            moderators.size();
        }
        Set<Account> subscribers = group.getSubscribers();
        if (subscribers != null) {
            subscribers.size();
        }
        Set<Account> requesters = group.getRequesters();
        if (requesters != null) {
            requesters.size();
        }
        logger.info("Completed method: read lazy membership collections");
    }

    private void updateStateOfMembership(Group source, Group destination) {
        logger.info("Started method: copy state of group's membership relations");
        destination.setModerators(source.getModerators());
        destination.setSubscribers(source.getSubscribers());
        destination.setRequesters(source.getRequesters());
        logger.info("Completed method: copy state of group's membership relations");
    }
}