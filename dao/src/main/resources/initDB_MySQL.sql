CREATE TABLE IF NOT EXISTS accounts
(
  user_id        BIGINT AUTO_INCREMENT,
  lastName       VARCHAR(128) NOT NULL,
  firstName      VARCHAR(128) NOT NULL,
  middleName     VARCHAR(128),
  birthDate      DATE,
  homeAddress    VARCHAR(128),
  workAddress    VARCHAR(128),
  email          VARCHAR(128) NOT NULL,
  icq            VARCHAR(128),
  skype          VARCHAR(128),
  additionalInfo VARCHAR(1024),
  password       VARCHAR(128) NOT NULL,
  avatar         MEDIUMBLOB,
  CONSTRAINT accounts_pk PRIMARY KEY (user_id),
  CONSTRAINT email_uq UNIQUE (email)
);

CREATE TABLE IF NOT EXISTS phone_types (
  type CHAR(4) PRIMARY KEY
);

CREATE TABLE IF NOT EXISTS phones
(
  phone_id    BIGINT AUTO_INCREMENT,
  account_id  BIGINT      NOT NULL,
  phoneNumber VARCHAR(30) NOT NULL,
  type        CHAR(4)     NOT NULL,
  CONSTRAINT phone_pk PRIMARY KEY (phone_id),
  CONSTRAINT accountId_fk FOREIGN KEY (account_id) REFERENCES accounts (user_id)
    ON DELETE CASCADE,
  CONSTRAINT phonetype_fk FOREIGN KEY (type) REFERENCES phone_types (type)
    ON DELETE CASCADE
);

CREATE TABLE IF NOT EXISTS groups
(
  group_id    BIGINT AUTO_INCREMENT,
  group_name  VARCHAR(128) NOT NULL,
  description VARCHAR(1024),
  avatar      MEDIUMBLOB,
  creator_id  BIGINT       NOT NULL,
  CONSTRAINT group_pk PRIMARY KEY (group_id),
  CONSTRAINT group_creator_fk FOREIGN KEY (creator_id) REFERENCES accounts (user_id)
    ON DELETE CASCADE,
  CONSTRAINT name_uq UNIQUE (group_name)
);

CREATE TABLE IF NOT EXISTS groups_moderators
(
  group_id     BIGINT NOT NULL,
  moderator_id BIGINT NOT NULL,

  CONSTRAINT groups_moderators_pk PRIMARY KEY (group_id, moderator_id),
  CONSTRAINT gm_group_fk FOREIGN KEY (group_id) REFERENCES groups (group_id)
    ON DELETE CASCADE,
  CONSTRAINT gm_moderator_fk FOREIGN KEY (moderator_id) REFERENCES accounts (user_id)
    ON DELETE CASCADE
);

CREATE TABLE IF NOT EXISTS groups_subscribers
(
  group_id      BIGINT NOT NULL,
  subscriber_id BIGINT NOT NULL,

  CONSTRAINT groups_subscribers_pk PRIMARY KEY (group_id, subscriber_id),
  CONSTRAINT gs_group_fk FOREIGN KEY (group_id) REFERENCES groups (group_id)
    ON DELETE CASCADE,
  CONSTRAINT gs_subscriber_fk FOREIGN KEY (subscriber_id) REFERENCES accounts (user_id)
    ON DELETE CASCADE
);

CREATE TABLE IF NOT EXISTS groups_requests
(
  group_id     BIGINT NOT NULL,
  requester_id BIGINT NOT NULL,

  CONSTRAINT groups_requests_pk PRIMARY KEY (group_id, requester_id),
  CONSTRAINT gr_group_fk FOREIGN KEY (group_id) REFERENCES groups (group_id)
    ON DELETE CASCADE,
  CONSTRAINT gr_requester_fk FOREIGN KEY (requester_id) REFERENCES accounts (user_id)
    ON DELETE CASCADE
);

CREATE TABLE IF NOT EXISTS friends
(
  first_account_id  BIGINT NOT NULL,
  second_account_id BIGINT NOT NULL,

  CONSTRAINT relations_pk PRIMARY KEY (first_account_id, second_account_id),
  CONSTRAINT first_account_fk FOREIGN KEY (first_account_id) REFERENCES accounts (user_id)
    ON DELETE CASCADE,
  CONSTRAINT second_account_fk FOREIGN KEY (second_account_id) REFERENCES accounts (user_id)
    ON DELETE CASCADE

);

CREATE TABLE IF NOT EXISTS requests_to_friends
(
  account_from_id BIGINT NOT NULL,
  account_to_id   BIGINT NOT NULL,

  CONSTRAINT friend_request_pk PRIMARY KEY (account_from_id, account_to_id),
  CONSTRAINT from_account_fk FOREIGN KEY (account_from_id) REFERENCES accounts (user_id)
    ON DELETE CASCADE,
  CONSTRAINT to_account_fk FOREIGN KEY (account_to_id) REFERENCES accounts (user_id)
    ON DELETE CASCADE
);

INSERT INTO phone_types VALUES ('HOME');
INSERT INTO phone_types VALUES ('WORK');