package com.getjavajob.training.web1701.dyominn.service;

import com.getjavajob.training.web1701.dyominn.common.models.Account;
import com.getjavajob.training.web1701.dyominn.common.models.Group;
import com.getjavajob.training.web1701.dyominn.dao.AccountDao;
import com.getjavajob.training.web1701.dyominn.dao.GroupDao;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import static java.time.LocalDate.parse;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class GroupServiceTest {
    @Mock
    private GroupDao groupDao;
    @Mock
    private AccountDao accountDao;
    @InjectMocks
    private GroupService groupService;
    private Group group1;
    private Group group2;
    private Account account1;
    private Account account2;

    public GroupServiceTest() {
        init();
    }

    @Test
    public void getById() {
        when(groupDao.get(2L)).thenReturn(group2);
        Group actual = groupService.get(2L);
        assertEquals(group2, actual);
    }

    @Test
    public void createGroup() {
        when(groupDao.insert(group2)).thenReturn(group2);
        Group actual = groupService.createGroup(group2);
        assertEquals(group2, actual);
    }

    @Test
    public void editGroup() {
        groupService.editGroup(group2);
        verify(groupDao).update(group2);
    }

    @Test
    public void deleteGroup() {
        groupService.deleteGroup(2L);
        verify(groupDao).delete(2L);
    }

    @Test
    public void removeGroupMember() {
        when(accountDao.get(2L)).thenReturn(account2);
        groupService.removeGroupMember(group2, 2L);
        verify(groupDao).removeGroupMember(group2, account2);
    }

    @Test
    public void dismissFromModerator() {
        when(accountDao.get(2L)).thenReturn(account2);
        groupService.dismissFromModerator(group2, 2L);
        verify(groupDao).dismissFromModerator(group2, account2);
    }

    @Test
    public void promoteToModerator() {
        when(accountDao.get(1L)).thenReturn(account1);
        groupService.promoteToModerator(group1, 1L);
        verify(groupDao).promoteToModerator(group1, account1);
    }

    @Test
    public void acceptRequest() {
        when(accountDao.get(1L)).thenReturn(account1);
        groupService.acceptRequest(group1, 1L);
        verify(groupDao).acceptRequest(group1, account1);
    }

    @Test
    public void declineRequest() {
        when(accountDao.get(2L)).thenReturn(account2);
        groupService.declineRequest(group2, 2L);
        verify(groupDao).declineRequest(group2, account2);
    }

    private void init() {
        account1 = new Account(1, "Ivanov", "Ivan", null, null, null, null, "ivanov@gmail.com", null, null, null, "111", null);
        account2 = new Account(2, "Ivanovich", "Petr", "Petrovich", parse("2000-01-01"), "Russia, Moscow, Red Square",
                "Russia, Moscow, Kremlin", "petrov@gmail.com", "petrov_icq", "petrov_skype", "petrov_additional_info", "123", null);
        group1 = new Group(1, account1, "Empty group", null);
        group2 = new Group(2, account1, "Java", "Java programming language");
    }
}