package com.getjavajob.training.web1701.dyominn.service;

import com.getjavajob.training.web1701.dyominn.common.models.Account;
import com.getjavajob.training.web1701.dyominn.common.models.Group;
import com.getjavajob.training.web1701.dyominn.dao.AccountDao;
import com.getjavajob.training.web1701.dyominn.dao.GroupDao;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;

import static java.time.LocalDate.parse;
import static java.util.Arrays.asList;
import static java.util.Collections.singletonList;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class AccountServiceTest {
    @Mock
    private AccountDao accountDao;
    @Mock
    private GroupDao groupDao;
    @InjectMocks
    private AccountService accountService;
    private Account account1;
    private Account account2;
    private Group group1;

    public AccountServiceTest() {
        init();
    }

    @Test
    public void getById() {
        when(accountDao.get(1L)).thenReturn(account1);
        Account actual = accountService.get(1L);
        assertEquals(account1, actual);
    }

    @Test
    public void getAccountForAuthorisation() {
        when(accountDao.getAccountForAuthorisation("ivanov@gmail.com", "111", false)).thenReturn(account1);
        Account actual = accountService.getAccountForAuthorisation("ivanov@gmail.com", "111", false);
        assertEquals(account1, actual);
    }

    @Test
    public void getAccountsByNameOrSurnamePatternAll() {
        List<Account> expected = new ArrayList<>(asList(account1, account2));
        when(accountDao.getAccountsByNameOrSurnamePattern("Ivan")).thenReturn(expected);
        List<Account> actual = accountService.getAccountsByNameOrSurnamePattern("Ivan");
        assertEquals(expected, actual);
    }

    @Test
    public void getAccountsByNameOrSurnamePatternLimited() {
        List<Account> expected = new ArrayList<>(singletonList(account1));
        when(accountDao.getAccountsByNameOrSurnamePattern("Ivan", 0, 1)).thenReturn(expected);
        List<Account> actual = accountService.getAccountsByNameOrSurnamePattern("Ivan", 0, 1);
        assertEquals(expected, actual);
    }

    @Test
    public void countAccountsWithPattern() {
        when(accountDao.countAccountsWithNameOrSurnamePattern("Ivan")).thenReturn(2L);
        long actual = accountService.countAccountsWithNameOrSurnamePattern("Ivan");
        assertEquals(2L, actual);
    }

    @Test
    public void createAccount() {
        when(accountDao.insert(account1)).thenReturn(account1);
        Account actual = accountService.createAccount(account1);
        assertEquals(account1, actual);
    }

    @Test
    public void editAccount() {
        accountService.editAccount(account1);
        verify(accountDao).update(account1);
    }

    @Test
    public void deleteAccount() {
        accountService.deleteAccount(1L);
        verify(accountDao).delete(1L);
    }

    @Test
    public void removeFromFriends() {
        accountService.removeFromFriends(1, 2, account1);
        verify(accountDao).removeFromFriends(1, 2, account1);
    }

    @Test
    public void acceptFriendshipRequest() {
        accountService.acceptFriendshipRequest(1, 2, account1);
        verify(accountDao).acceptFriendshipRequest(1, 2, account1);
    }

    @Test
    public void declineFriendshipRequest() {
        accountDao.declineFriendshipRequest(2, 1, account2);
        verify(accountDao).declineFriendshipRequest(2, 1, account2);
    }

    @Test
    public void cancelOutgoingRequest() {
        accountDao.cancelOutgoingRequest(1, 2, account1);
        verify(accountDao).cancelOutgoingRequest(1, 2, account1);
    }

    @Test
    public void sendFriendshipRequest() {
        accountDao.sendFriendshipRequest(1, 2, account1);
        verify(accountDao).sendFriendshipRequest(1, 2, account1);
    }

    @Test
    public void sendGroupRequest() {
        when(groupDao.get(1L)).thenReturn(group1);
        accountService.sendGroupRequest(account2, 1L);
        verify(accountDao).sendGroupRequest(account2, group1);
    }

    @Test
    public void leaveGroup() {
        when(groupDao.get(1L)).thenReturn(group1);
        accountService.leaveGroup(account2, 1L);
        verify(accountDao).leaveGroup(account2, group1);
    }

    @Test
    public void cancelRequestToGroup() {
        when(groupDao.get(1L)).thenReturn(group1);
        accountService.cancelRequestToGroup(account2, 1L);
        verify(accountDao).cancelRequestToGroup(account2, group1);
    }

    private void init() {
        account1 = new Account(1, "Ivanov", "Ivan", null, null, null, null, "ivanov@gmail.com", null, null, null, "111", null);
        account2 = new Account(2, "Ivanovich", "Petr", "Petrovich", parse("2000-01-01"), "Russia, Moscow, Red Square",
                "Russia, Moscow, Kremlin", "petrov@gmail.com", "petrov_icq", "petrov_skype", "petrov_additional_info", "123", null);
        group1 = new Group(1, account1, "Empty group", null);
    }
}