package com.getjavajob.training.web1701.dyominn.service;

import com.getjavajob.training.web1701.dyominn.common.models.Account;
import com.getjavajob.training.web1701.dyominn.common.models.Group;
import com.getjavajob.training.web1701.dyominn.dao.AccountDao;
import com.getjavajob.training.web1701.dyominn.dao.GroupDao;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class AccountService {
    private static final Logger logger = LoggerFactory.getLogger(AccountService.class);
    private final AccountDao accountDao;
    private final GroupDao groupDao;

    @Autowired
    public AccountService(AccountDao accountDao, GroupDao groupDao) {
        this.accountDao = accountDao;
        this.groupDao = groupDao;
    }

    @Transactional(readOnly = true)
    public Account get(long id) {
        logger.info("Transaction started: get account by id");
        Account account = accountDao.get(id);
        logger.info("Transaction completed: get account by id");
        return account;
    }

    @Transactional(readOnly = true)
    public Account getAccountForAuthorisation(String email, String password, boolean passedPasswordAlreadyHashed) {
        logger.info("Transaction started: get account by email and password/hash");
        Account account = accountDao.getAccountForAuthorisation(email, password, passedPasswordAlreadyHashed);
        logger.info("Transaction completed: get account by email and password/hash");
        return account;
    }

    @Transactional(readOnly = true)
    public List<Account> getAccountsByNameOrSurnamePattern(String pattern) {
        logger.info("Transaction started: get list of all accounts with name/surname pattern");
        List<Account> accounts = accountDao.getAccountsByNameOrSurnamePattern(pattern);
        logger.info("Transaction completed: get list of all accounts with name/surname pattern");
        return accounts;
    }

    @Transactional(readOnly = true)
    public List<Account> getAccountsByNameOrSurnamePattern(String pattern, int startIndex, int accountsQuantity) {
        logger.info("Transaction started: get fixed list of accounts with name/surname pattern");
        List<Account> accounts = accountDao.getAccountsByNameOrSurnamePattern(pattern, startIndex, accountsQuantity);
        logger.info("Transaction completed: get fixed list of accounts with name/surname pattern");
        return accounts;
    }

    @Transactional(readOnly = true)
    public long countAccountsWithNameOrSurnamePattern(String pattern) {
        logger.info("Transaction started: count accounts with name/surname pattern");
        long accounts = accountDao.countAccountsWithNameOrSurnamePattern(pattern);
        logger.info("Transaction completed: count accounts with name/surname pattern");
        return accounts;
    }

    @Transactional
    public Account createAccount(Account account) {
        logger.info("Transaction started: create new account");
        Account inserted = accountDao.insert(account);
        logger.info("Transaction completed: create new account");
        return inserted;
    }

    @Transactional
    public void editAccount(Account account) {
        logger.info("Transaction started: update account");
        accountDao.update(account);
        logger.info("Transaction completed: update account");
    }

    @Transactional
    public void deleteAccount(long id) {
        logger.info("Transaction started: delete account by id");
        accountDao.delete(id);
        logger.info("Transaction completed: delete account by id");
    }

    // Friends relationship logic

    @Transactional
    public void removeFromFriends(long initiatorFriendId, long removableFriendId, Account sessionAccountForUpdate) {
        logger.info("Transaction started: remove account from friends");
        if (initiatorFriendId != removableFriendId) {
            accountDao.removeFromFriends(initiatorFriendId, removableFriendId, sessionAccountForUpdate);
        }
        logger.info("Transaction completed: remove account from friends");
    }

    @Transactional
    public void acceptFriendshipRequest(long receiverId, long requesterId, Account sessionAccountForUpdate) {
        logger.info("Transaction started: accept friendship request");
        if (receiverId != requesterId) {
            accountDao.acceptFriendshipRequest(receiverId, requesterId, sessionAccountForUpdate);
        }
        logger.info("Transaction completed: accept friendship request");
    }

    @Transactional
    public void declineFriendshipRequest(long receiverId, long requesterId, Account sessionAccountForUpdate) {
        logger.info("Transaction started: decline friendship request");
        if (receiverId != requesterId) {
            accountDao.declineFriendshipRequest(receiverId, requesterId, sessionAccountForUpdate);
        }
        logger.info("Transaction completed: decline friendship request");
    }

    @Transactional
    public void cancelOutgoingRequest(long requesterId, long receiverId, Account sessionAccountForUpdate) {
        logger.info("Transaction started: cancel outgoing friendship request");
        if (receiverId != requesterId) {
            accountDao.cancelOutgoingRequest(requesterId, receiverId, sessionAccountForUpdate);
        }
        logger.info("Transaction completed: cancel outgoing friendship request");
    }

    @Transactional
    public void sendFriendshipRequest(long requesterId, long receiverId, Account sessionAccountForUpdate) {
        logger.info("Transaction started: send outgoing friendship request");
        if (receiverId != requesterId) {
            accountDao.sendFriendshipRequest(requesterId, receiverId, sessionAccountForUpdate);
        }
        logger.info("Transaction completed: send outgoing friendship request");
    }

    // Account's groups logic

    @Transactional
    public void sendGroupRequest(Account requester, long groupId) {
        logger.info("Transaction started: send request to group");
        Group group = groupDao.get(groupId);
        accountDao.sendGroupRequest(requester, group);
        logger.info("Transaction completed: send request to group");
    }

    @Transactional
    public void leaveGroup(Account member, long groupId) {
        logger.info("Transaction started: leave group");
        Group group = groupDao.get(groupId);
        accountDao.leaveGroup(member, group);
        logger.info("Transaction completed: leave group");
    }

    @Transactional
    public void cancelRequestToGroup(Account requester, long groupId) {
        logger.info("Transaction started: cancel request to group");
        Group group = groupDao.get(groupId);
        accountDao.cancelRequestToGroup(requester, group);
        logger.info("Transaction completed: cancel request to group");
    }
}