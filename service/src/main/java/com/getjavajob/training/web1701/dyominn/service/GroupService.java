package com.getjavajob.training.web1701.dyominn.service;

import com.getjavajob.training.web1701.dyominn.common.models.Account;
import com.getjavajob.training.web1701.dyominn.common.models.Group;
import com.getjavajob.training.web1701.dyominn.dao.AccountDao;
import com.getjavajob.training.web1701.dyominn.dao.GroupDao;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class GroupService {
    private static final Logger logger = LoggerFactory.getLogger(GroupService.class);
    private final GroupDao groupDao;
    private final AccountDao accountDao;

    @Autowired
    public GroupService(GroupDao groupDao, AccountDao accountDao) {
        this.groupDao = groupDao;
        this.accountDao = accountDao;
    }

    @Transactional(readOnly = true)
    public Group get(long id) {
        logger.info("Transaction started: get group by id");
        Group group = groupDao.get(id);
        logger.info("Transaction completed: get group by id");
        return group;
    }

    @Transactional
    public Group createGroup(Group group) {
        logger.info("Transaction started: create new group");
        Group created = groupDao.insert(group);
        logger.info("Transaction completed: create new group");
        return created;
    }

    @Transactional
    public void editGroup(Group group) {
        logger.info("Transaction started: update group");
        groupDao.update(group);
        logger.info("Transaction completed: update group");
    }

    @Transactional
    public void deleteGroup(long id) {
        logger.info("Transaction started: delete group");
        groupDao.delete(id);
        logger.info("Transaction completed: delete group");
    }

    @Transactional
    public void removeGroupMember(Group group, long memberId) {
        logger.info("Transaction started: delete group's member");
        Account member = accountDao.get(memberId);
        groupDao.removeGroupMember(group, member);
        logger.info("Transaction completed: delete group's member");
    }

    @Transactional
    public void dismissFromModerator(Group group, long memberId) {
        logger.info("Transaction started: dismiss moderator");
        Account member = accountDao.get(memberId);
        groupDao.dismissFromModerator(group, member);
        logger.info("Transaction completed: dismiss moderator");
    }

    @Transactional
    public void promoteToModerator(Group group, long memberId) {
        logger.info("Transaction started: promote subscriber to moderator");
        Account member = accountDao.get(memberId);
        groupDao.promoteToModerator(group, member);
        logger.info("Transaction completed: promote subscriber to moderator");
    }

    @Transactional
    public void acceptRequest(Group group, long requesterId) {
        logger.info("Transaction started: accept request to group");
        Account requester = accountDao.get(requesterId);
        groupDao.acceptRequest(group, requester);
        logger.info("Transaction completed: accept request to group");
    }

    @Transactional
    public void declineRequest(Group group, long requesterId) {
        logger.info("Transaction started: decline request to group");
        Account requester = accountDao.get(requesterId);
        groupDao.declineRequest(group, requester);
        logger.info("Transaction completed: decline request to group");
    }
}