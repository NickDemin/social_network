package com.getjavajob.training.web1701.dyominn.webapp.interceptors;

import com.getjavajob.training.web1701.dyominn.common.models.Account;
import com.getjavajob.training.web1701.dyominn.service.AccountService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import static com.getjavajob.training.web1701.dyominn.webapp.utils.ImageBytesEncoder.getAccountAvatarString;
import static java.lang.Boolean.TRUE;

public class AuthorizationInterceptor extends HandlerInterceptorAdapter {
    private static final Logger logger = LoggerFactory.getLogger(AuthorizationInterceptor.class);
    private AccountService accountService;

    @Autowired
    public AuthorizationInterceptor(AccountService accountService) {
        this.accountService = accountService;
    }

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        logger.debug("Start to pre-handle the request with URI: {}", request.getRequestURI());
        HttpSession session = request.getSession();
        String requestURI = request.getRequestURI();
        String contextPath = request.getContextPath();
        Account authorizedAccount = (Account) session.getAttribute("authorizedAccount");
        if (authorizedAccount != null && isURINotAcceptableForAuthorizedAccount(contextPath, requestURI)) {
            logger.debug("Account is authorized, redirect to account's page.");
            response.sendRedirect(contextPath + "/id" + authorizedAccount.getId());
            return false;
        }
        if (authorizedAccount == null && isURIAcceptableForAuthorizationByCookies(contextPath, requestURI)) {
            logger.debug("Account is not authorized, try to authorize by cookies");
            Account account = getAccountByCookies(request);
            if (account != null) {
                session.setAttribute("authorizedAccount", account);
                session.setAttribute("authorizedAccountAvatar", getAccountAvatarString(account.getAvatar(), contextPath));
                session.setAttribute("rememberedAccount", TRUE); // flag of checkbox 'rememberMe'
                logger.debug("Successfully authorized by cookies");
            } else {
                logger.debug("Cannot authorize by cookies. Redirect to login page");
                response.sendRedirect(contextPath + "/login");
                return false;
            }
        }
        logger.debug("Complete to pre-handle the request with URI: {}; Proceed executing chain", requestURI);
        return true;
    }

    /**
     * returns Account instance from database by cookies (email and hashed password),
     * or null if no such cookies or account for them in database.
     */
    private Account getAccountByCookies(HttpServletRequest request) {
        String email = "";
        String hash = "";
        Cookie[] cookies = request.getCookies();
        if (cookies != null) {
            for (Cookie cookie : cookies) {
                String name = cookie.getName();
                if (name.equals("email")) {
                    email = cookie.getValue();
                }
                if (name.equals("hash")) {
                    hash = cookie.getValue();
                }
            }
        }
        return accountService.getAccountForAuthorisation(email, hash, true);
    }

    private boolean isURIAcceptableForAuthorizationByCookies(String contextPath, String requestURI) {
        return !requestURI.equals(contextPath + "/login") && !requestURI.equals(contextPath + "/accountValidationHandler") &&
                !requestURI.equals(contextPath + "/registration") && !requestURI.equals(contextPath + "/registrationHandler") &&
                !requestURI.startsWith(contextPath + "/bootstrap/") && !requestURI.startsWith(contextPath + "/css/") &&
                !requestURI.startsWith(contextPath + "/scripts/") && !requestURI.startsWith(contextPath + "/images/");
    }

    private boolean isURINotAcceptableForAuthorizedAccount(String contextPath, String requestURI) {
        return requestURI.equals(contextPath + "/") || requestURI.equals(contextPath + "/login") || requestURI.equals(contextPath + "/accountValidationHandler") ||
                requestURI.equals(contextPath + "/registration") || requestURI.equals(contextPath + "/registrationHandler");
    }
}