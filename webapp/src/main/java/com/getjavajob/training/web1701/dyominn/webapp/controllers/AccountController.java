package com.getjavajob.training.web1701.dyominn.webapp.controllers;

import com.getjavajob.training.web1701.dyominn.common.models.Account;
import com.getjavajob.training.web1701.dyominn.common.models.Phone;
import com.getjavajob.training.web1701.dyominn.service.AccountService;
import com.getjavajob.training.web1701.dyominn.webapp.utils.AccountPhonePropertyEditor;
import com.getjavajob.training.web1701.dyominn.webapp.utils.LocalDatePropertyEditor;
import com.getjavajob.training.web1701.dyominn.webapp.utils.NonEmptyByteArrayMultipartFileEditor;
import com.thoughtworks.xstream.XStream;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import static com.getjavajob.training.web1701.dyominn.common.models.PhoneType.HOME;
import static com.getjavajob.training.web1701.dyominn.common.models.PhoneType.WORK;
import static com.getjavajob.training.web1701.dyominn.common.utils.CryptPassword.hash;
import static com.getjavajob.training.web1701.dyominn.webapp.utils.AccountXmlSerialization.accountXmlIsValid;
import static com.getjavajob.training.web1701.dyominn.webapp.utils.AccountXmlSerialization.getXstreamForAccount;
import static com.getjavajob.training.web1701.dyominn.webapp.utils.ImageBytesEncoder.getAccountAvatarString;
import static java.lang.Boolean.TRUE;
import static org.springframework.http.HttpHeaders.CONTENT_DISPOSITION;
import static org.springframework.http.MediaType.MULTIPART_FORM_DATA_VALUE;
import static org.springframework.http.MediaType.TEXT_XML;

@Controller
public class AccountController {
    private static final Logger logger = LoggerFactory.getLogger(AccountController.class);
    private final AccountService accountService;

    @Autowired
    public AccountController(AccountService accountService) {
        this.accountService = accountService;
    }

    @InitBinder
    public void editUnusualInputObjects(WebDataBinder binder) {
        binder.registerCustomEditor(byte[].class, "avatar", new NonEmptyByteArrayMultipartFileEditor());
        binder.registerCustomEditor(Phone.class, "personalPhones", new AccountPhonePropertyEditor(HOME));
        binder.registerCustomEditor(Phone.class, "workPhones", new AccountPhonePropertyEditor(WORK));
        binder.registerCustomEditor(LocalDate.class, "birthDate", new LocalDatePropertyEditor());
    }

    @GetMapping("/id{accountId}")
    public ModelAndView displayAccount(@PathVariable("accountId") long id,
                                       @SessionAttribute("authorizedAccount") Account authorizedAccount,
                                       HttpSession session,
                                       HttpServletRequest request) {
        logger.info("Received a request to display account with id: {}", id);
        Account accountToDisplay = accountService.get(id);
        if (logger.isDebugEnabled()) {
            logger.debug("Received an account for id {}: {}", id, accountToDisplay);
        }
        ModelAndView modelAndView = new ModelAndView("account");
        modelAndView.addObject("displayedAccount", accountToDisplay);
        modelAndView.addObject("displayedAvatar", getAccountAvatarString(accountToDisplay.getAvatar(), request.getContextPath()));
        if (authorizedAccount.getId() == id) {
            session.setAttribute("authorizedAccount", accountToDisplay);
            modelAndView.addObject("isMyPage", TRUE);
            modelAndView.addObject("friendshipRequests", accountToDisplay.getIncomingFriendRequests().size());
        } else {
            if (accountToDisplay.getFriends().contains(authorizedAccount)) {
                modelAndView.addObject("isMyFriend", TRUE);
            }
            if (accountToDisplay.getIncomingFriendRequests().contains(authorizedAccount)) {
                modelAndView.addObject("isMySubscription", TRUE);
            }
        }
        logger.info("Return a model of account with id: {} to view", id);
        return modelAndView;
    }

    @PostMapping(value = "/registrationHandler", consumes = {MULTIPART_FORM_DATA_VALUE})
    public String registerAccount(@ModelAttribute Account account, HttpSession session) {
        logger.info("Received a request to register new Account");
        if (logger.isDebugEnabled()) {
            logger.debug("Account model received: {}", account);
        }
        account.setPassword(hash(account.getPassword())); // Change password on it's hash
        mapAccountPhonesOwner(account);
        accountService.createAccount(account);
        session.setAttribute("successfullyRegistered", TRUE);
        logger.info("Account successfully registered. Redirecting to login page");
        return "redirect:/login";
    }

    @PostMapping(value = "/editAccountHandler", consumes = {MULTIPART_FORM_DATA_VALUE})
    public String editAccount(@ModelAttribute Account updatedAccount,
                              @SessionAttribute("authorizedAccount") Account sessionAccount,
                              @SessionAttribute(value = "rememberedAccount", required = false) String rememberedAccountFlag,
                              HttpSession session,
                              HttpServletRequest request,
                              HttpServletResponse response) {
        logger.info("Received a request to update Account");
        if (logger.isDebugEnabled()) {
            logger.debug("Updated account model received: {}", updatedAccount);
        }
        logger.debug("Update session model of account");
        //update basic info
        sessionAccount.setLastName(updatedAccount.getLastName());
        sessionAccount.setFirstName(updatedAccount.getFirstName());
        sessionAccount.setMiddleName(updatedAccount.getMiddleName());
        sessionAccount.setBirthDate(updatedAccount.getBirthDate());
        sessionAccount.setHomeAddress(updatedAccount.getHomeAddress());
        sessionAccount.setWorkAddress(updatedAccount.getWorkAddress());
        sessionAccount.setEmail(updatedAccount.getEmail());
        sessionAccount.setIcq(updatedAccount.getIcq());
        sessionAccount.setSkype(updatedAccount.getSkype());
        sessionAccount.setAdditionalInfo(updatedAccount.getAdditionalInfo());

        //update phones
        List<Phone> updatedPersonalPhones = updatedAccount.getPersonalPhones();
        List<Phone> updatedWorkPhones = updatedAccount.getWorkPhones();
        sessionAccount.setPersonalPhones(updatedPersonalPhones != null ? updatedPersonalPhones : new ArrayList<>());
        sessionAccount.setWorkPhones(updatedWorkPhones != null ? updatedWorkPhones : new ArrayList<>());
        mapAccountPhonesOwner(sessionAccount);

        //update password if changed
        String newPassword = updatedAccount.getPassword();
        if (newPassword != null && !newPassword.isEmpty()) {
            String newHashedPassword = hash(newPassword);
            sessionAccount.setPassword(newHashedPassword);
            //update hash in cookies if needed
            if (rememberedAccountFlag != null) {
                Cookie newHashCookie = new Cookie("hash", newHashedPassword);
                newHashCookie.setMaxAge((int) TimeUnit.DAYS.toMinutes(30));
                response.addCookie(newHashCookie);
            }
        }
        //update avatar
        byte[] newAvatar = updatedAccount.getAvatar();
        if (newAvatar != null) {
            sessionAccount.setAvatar(newAvatar);
            session.setAttribute("authorizedAccountAvatar", getAccountAvatarString(newAvatar, request.getContextPath()));
        }
        accountService.editAccount(sessionAccount);
        logger.info("Account successfully updated. Redirecting to account page");
        return "redirect:/id" + sessionAccount.getId();
    }

    @PostMapping(value = "/editAccountByXmlHandler")
    public ModelAndView editAccountFormByXml(@RequestParam("xmlAccount") String accountXmlText) {
        logger.info("Received a request to fill account's edit form from XML file");
        ModelAndView modelAndView = new ModelAndView("accountEdit");
        if (accountXmlText == null || accountXmlText.isEmpty()) {
            modelAndView.addObject("xmlFileException", "XML file is null/empty");
            return modelAndView;
        }
        if (!accountXmlIsValid(accountXmlText)) {
            modelAndView.addObject("xmlFileException", "XML file is invalid");
            return modelAndView;
        }
        XStream xstream = getXstreamForAccount();
        Account fromXml = (Account) xstream.fromXML(accountXmlText);
        if (fromXml.getPersonalPhones().size() > 4 || fromXml.getWorkPhones().size() > 4) {
            modelAndView.addObject("xmlFileException", "Illegal quantity of phones in XML. Max qty for group is: 4");
            return modelAndView;
        }
        modelAndView.addObject("accountFromXml", fromXml);
        logger.info("Account's edit form successfully filled from XML file");
        return modelAndView;
    }

    @GetMapping(value = "/saveAccountAsXml")
    public HttpEntity<byte[]> saveAccountAsXml(@SessionAttribute("authorizedAccount") Account authorizedAccount) {
        logger.info("Received a request to save account as XML file");
        XStream xStream = getXstreamForAccount();
        String xmlDeclaration = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n";
        String accountDataXml = xStream.toXML(authorizedAccount);
        byte[] accountBytes = (xmlDeclaration + accountDataXml).getBytes();
        HttpHeaders header = new HttpHeaders();
        header.setContentType(TEXT_XML);
        header.set(CONTENT_DISPOSITION, "attachment; filename=account_" + authorizedAccount.getId() + ".xml");
        header.setContentLength(accountBytes.length);
        logger.info("Account successfully converted to XML file");
        return new HttpEntity<>(accountBytes, header);
    }

    // Accounts' relationship logic

    @GetMapping("/friends")
    public ModelAndView displayAccountFriendship(@RequestParam("id") long accountId,
                                                 @SessionAttribute("authorizedAccount") Account authorizedAccount) {
        logger.info("Received a request to display friends of account with id {}", accountId);
        Account account = accountService.get(accountId);
        if (logger.isDebugEnabled()) {
            logger.debug("Selected an account model from database: {}", account);
        }
        ModelAndView modelAndView = new ModelAndView("accountFriendship");
        if (authorizedAccount.getId() == accountId) {
            modelAndView.addObject("isMyFriends", TRUE);
        }
        modelAndView.addObject("friends", account.getFriends());
        modelAndView.addObject("incomingRequests", account.getIncomingFriendRequests());
        modelAndView.addObject("outgoingRequests", account.getOutcomingFriendRequests());
        logger.info("Forward to the view with account's friends model");
        return modelAndView;
    }

    @GetMapping("/remove")
    public String removeFromFriends(@RequestParam("friend") long friendId,
                                    @SessionAttribute("authorizedAccount") Account initiator,
                                    HttpServletRequest request) {
        logger.info("Received a request to remove account from friends");
        accountService.removeFromFriends(initiator.getId(), friendId, initiator);
        logger.info("Successfully removed from friends. Redirect to the previous page.");
        return "redirect:" + request.getHeader("Referer");
    }

    @GetMapping("/acceptFriendship")
    public String acceptFriendshipRequest(@RequestParam("id") long requesterId,
                                          @SessionAttribute("authorizedAccount") Account receiver,
                                          HttpServletRequest request) {
        logger.info("Received acceptance for friendship request");
        accountService.acceptFriendshipRequest(receiver.getId(), requesterId, receiver);
        logger.info("Friendship request successfully accepted. Redirect to the previous page.");
        return "redirect:" + request.getHeader("Referer");
    }

    @GetMapping("/declineFriendship")
    public String declineFriendshipRequest(@RequestParam("id") long requesterId,
                                           @SessionAttribute("authorizedAccount") Account receiver,
                                           HttpServletRequest request) {
        logger.info("Received rejection of friendship request");
        accountService.declineFriendshipRequest(receiver.getId(), requesterId, receiver);
        logger.info("Friendship request successfully rejected. Redirect to the previous page.");
        return "redirect:" + request.getHeader("Referer");
    }

    @GetMapping("/cancelOutgoingFriendship")
    public String cancelOutgoingFriendshipRequest(@RequestParam("id") long receiverId,
                                                  @SessionAttribute("authorizedAccount") Account requester,
                                                  HttpServletRequest request) {
        logger.info("Received cancellation of outgoing friendship request");
        accountService.cancelOutgoingRequest(requester.getId(), receiverId, requester);
        logger.info("Outgoing friendship request successfully cancelled. Redirect to the previous page.");
        return "redirect:" + request.getHeader("Referer");
    }

    @GetMapping("/offerFriendship")
    public String sendFriendshipRequest(@RequestParam("id") long receiverId,
                                        @SessionAttribute("authorizedAccount") Account requester,
                                        HttpServletRequest request) {
        logger.info("Received a request to send outgoing friendship request");
        accountService.sendFriendshipRequest(requester.getId(), receiverId, requester);
        logger.info("Outgoing friendship request successfully sent. Redirect to the previous page.");
        return "redirect:" + request.getHeader("Referer");
    }

    // Accounts' groups logic

    @GetMapping("/groups")
    public ModelAndView displayAccountGroups(@RequestParam("id") long accountId,
                                             @SessionAttribute("authorizedAccount") Account authorizedAccount) {
        logger.info("Received a request to display groups of account with id {}", accountId);
        Account account = accountService.get(accountId);
        if (logger.isDebugEnabled()) {
            logger.debug("Selected an account model from database: {}", account);
        }
        ModelAndView modelAndView = new ModelAndView("accountGroups");
        if (authorizedAccount.getId() == accountId) {
            modelAndView.addObject("isMyGroups", TRUE);
        }
        modelAndView.addObject("createdGroups", account.getCreatedGroups());
        modelAndView.addObject("moderatedGroups", account.getModeratedGroups());
        modelAndView.addObject("requestedGroups", account.getRequestedGroups());
        modelAndView.addObject("subscriptions", account.getSubscriptions());
        logger.info("Forward to the view with account's groups model");
        return modelAndView;
    }

    @GetMapping("/leaveGroup")
    public String leaveGroup(@RequestParam("id") long groupId,
                             @SessionAttribute("authorizedAccount") Account authorizedAccount,
                             HttpServletRequest request) {
        logger.info("Received a request to leave the group");
        accountService.leaveGroup(authorizedAccount, groupId);
        logger.info("Successfully unsubscribed from group. Redirect to the previous page.");
        return "redirect:" + request.getHeader("Referer");
    }

    @GetMapping("/cancelGroupRequest")
    public String cancelGroupRequest(@RequestParam("id") long groupId,
                                     @SessionAttribute("authorizedAccount") Account authorizedAccount,
                                     HttpServletRequest request) {
        logger.info("Received cancellation of outgoing group request");
        accountService.cancelRequestToGroup(authorizedAccount, groupId);
        logger.info("Outgoing group request successfully cancelled. Redirect to the previous page.");
        return "redirect:" + request.getHeader("Referer");
    }

    @GetMapping(value = "/sendGroupRequest")
    public String sendGroupRequest(@RequestParam("id") long groupId,
                                   @SessionAttribute("authorizedAccount") Account authorizedAccount,
                                   HttpServletRequest request) {
        logger.info("Received a request to send outgoing group request");
        accountService.sendGroupRequest(authorizedAccount, groupId);
        logger.info("Request to the group successfully sent. Redirect to the prevoious page.");
        return "redirect:" + request.getHeader("Referer");
    }

    // Utils

    private void mapAccountPhonesOwner(Account account) {
        List<Phone> personalPhones = account.getPersonalPhones();
        List<Phone> workPhones = account.getWorkPhones();
        if (personalPhones != null) {
            for (Phone phone : personalPhones) {
                phone.setOwner(account);
            }
        }
        if (workPhones != null) {
            for (Phone phone : workPhones) {
                phone.setOwner(account);
            }
        }
    }
}