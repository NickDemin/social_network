package com.getjavajob.training.web1701.dyominn.webapp.utils;


import static org.apache.commons.codec.binary.Base64.encodeBase64String;

public class ImageBytesEncoder {

    public static String getAccountAvatarString(byte[] avatarBytes, String contextPath) {
        if (avatarBytes != null) {
            return "data:image/jpg;base64," + encodeBase64String(avatarBytes);
        } else {
            return contextPath + "/images/avatar-placeholder.png";
        }
    }

    public static String getGroupAvatarString(byte[] avatarBytes, String contextPath) {
        if (avatarBytes != null) {
            return "data:image/jpg;base64," + encodeBase64String(avatarBytes);
        } else {
            return contextPath + "/images/group-avatar-placeholder.png";
        }
    }
}