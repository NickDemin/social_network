package com.getjavajob.training.web1701.dyominn.webapp.controllers;

import com.getjavajob.training.web1701.dyominn.common.models.Account;
import com.getjavajob.training.web1701.dyominn.service.AccountService;
import com.getjavajob.training.web1701.dyominn.webapp.dto.AccountDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

import static com.getjavajob.training.web1701.dyominn.webapp.dto.DTOAssembler.convertToDtoList;

@Controller
public class SearchController {
    private static final Logger logger = LoggerFactory.getLogger(SearchController.class);
    private AccountService accountService;

    @Autowired
    public SearchController(AccountService accountService) {
        this.accountService = accountService;
    }

    @GetMapping("/search")
    public ModelAndView searchAccounts(@RequestParam(value = "query", required = false) String searchQuery) {
        logger.info("Received request to found accounts with pattern: {}", searchQuery);
        long accounts = accountService.countAccountsWithNameOrSurnamePattern(searchQuery);
        ModelAndView modelAndView = new ModelAndView("searchResults");
        modelAndView.addObject("accountsQuantityFound", accounts);
        modelAndView.addObject("searchQuery", searchQuery);
        logger.info("Forward to view with search results");
        return modelAndView;
    }

    @GetMapping("/searchPaginationHandler")
    @ResponseBody
    public List<AccountDTO> searchAccountsWithPagination(@RequestParam(value = "query", required = false) String searchQuery,
                                                         @RequestParam("page") int page,
                                                         @RequestParam("rows") int countPerPage,
                                                         HttpServletRequest request) {
        logger.info("Received request for found accounts for particular page");
        int startIndex = (page - 1) * countPerPage;
        List<Account> pageAccounts = accountService.getAccountsByNameOrSurnamePattern(searchQuery, startIndex, countPerPage);
        logger.info("Return model with results to response body.");
        return convertToDtoList(pageAccounts, request.getContextPath());
    }

    @GetMapping("/searchAutocompleteHandler")
    @ResponseBody
    public List<AccountDTO> getAccountsByPattern(@RequestParam("searchPattern") String searchPattern,
                                                 HttpServletRequest request) {
        logger.info("Received request for autocomplete data for pattern: {}", searchPattern);
        List<Account> accounts = accountService.getAccountsByNameOrSurnamePattern(searchPattern, 0, 6);
        logger.debug("Founded {} accounts for pattern {}", accounts.size(), searchPattern);
        logger.info("Return results to autocomplete block");
        return convertToDtoList(accounts, request.getContextPath());
    }
}