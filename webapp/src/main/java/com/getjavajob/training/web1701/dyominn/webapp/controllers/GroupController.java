package com.getjavajob.training.web1701.dyominn.webapp.controllers;

import com.getjavajob.training.web1701.dyominn.common.models.Account;
import com.getjavajob.training.web1701.dyominn.common.models.Group;
import com.getjavajob.training.web1701.dyominn.service.GroupService;
import com.getjavajob.training.web1701.dyominn.webapp.utils.NonEmptyByteArrayMultipartFileEditor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.Set;

import static com.getjavajob.training.web1701.dyominn.webapp.utils.ImageBytesEncoder.getGroupAvatarString;
import static java.lang.Boolean.TRUE;
import static org.springframework.http.MediaType.MULTIPART_FORM_DATA_VALUE;

@Controller
public class GroupController {
    private static final Logger logger = LoggerFactory.getLogger(GroupController.class);
    private GroupService groupService;

    @Autowired
    public GroupController(GroupService groupService) {
        this.groupService = groupService;
    }

    @InitBinder
    public void editUnusualInputObjects(WebDataBinder binder) {
        binder.registerCustomEditor(byte[].class, "avatar", new NonEmptyByteArrayMultipartFileEditor());
    }

    @GetMapping("/group{groupId}")
    public ModelAndView displayGroupById(@PathVariable("groupId") long groupId,
                                         @SessionAttribute("authorizedAccount") Account authorizedAccount,
                                         HttpServletRequest request,
                                         HttpSession session) {
        logger.info("Received a request to display group with id: {}", groupId);
        Group group = groupService.get(groupId);
        if (logger.isDebugEnabled()) {
            logger.debug("Selected the group: {} ", group);
        }
        session.setAttribute("currentGroup", group);
        session.setAttribute("currentGroupAvatar", getGroupAvatarString(group.getAvatar(), request.getContextPath()));

        ModelAndView modelAndView = new ModelAndView("group");
        if (group.getCreator().equals(authorizedAccount)) {
            modelAndView.addObject("isCreator", TRUE);
        }
        if (hasModeratorRights(authorizedAccount, group)) {
            modelAndView.addObject("isModerator", TRUE);
        }
        if (hasModeratorRights(authorizedAccount, group) || group.getSubscribers().contains(authorizedAccount)) {
            modelAndView.addObject("isMember", TRUE);
        }
        if (group.getRequesters().contains(authorizedAccount)) {
            modelAndView.addObject("isRequester", TRUE);
        }
        logger.info("Return a model of group with id {} to view", groupId);
        return modelAndView;
    }

    @GetMapping("/members")
    public ModelAndView displayGroupMembership(@RequestParam("group") long groupId,
                                               @SessionAttribute("authorizedAccount") Account authorizedAccount,
                                               HttpSession session) {
        logger.info("Received a request to display members of group with id {}", groupId);
        Group group = groupService.get(groupId);
        if (logger.isDebugEnabled()) {
            logger.debug("Selected an group model from database: {}", group);
        }
        session.setAttribute("currentGroup", group); // needed when this handler called not from group itself
        ModelAndView modelAndView = new ModelAndView("groupMembership");
        if (hasModeratorRights(authorizedAccount, group)) {
            modelAndView.addObject("isModerator", TRUE);
        }
        modelAndView.addObject("moderators", group.getModerators());
        modelAndView.addObject("subscribers", group.getSubscribers());
        modelAndView.addObject("requesters", group.getRequesters());
        logger.info("Forward to the view with group's members model");
        return modelAndView;
    }

    @PostMapping(value = "/groupRegistrationHandler", consumes = {MULTIPART_FORM_DATA_VALUE})
    public String registerGroup(@ModelAttribute Group group,
                                @SessionAttribute("authorizedAccount") Account authorizedAccount) {
        logger.info("Received a request to register the new Group");
        if (logger.isDebugEnabled()) {
            logger.debug("Group model received: {}", group);
        }
        group.setCreator(authorizedAccount);
        groupService.createGroup(group);
        logger.debug("Group successfully registered. Redirect to account's groups page.");
        return "redirect:/groups?id=" + authorizedAccount.getId();
    }

    @GetMapping("/editGroup")
    public ModelAndView checkPermissionsToEditGroup(@SessionAttribute("currentGroup") Group currentGroup,
                                                    @SessionAttribute("authorizedAccount") Account authorizedAccount,
                                                    HttpServletRequest request) {
        logger.info("Received a request for editing the group. Check permissions");
        ModelAndView modelAndView;
        if (hasModeratorRights(authorizedAccount, currentGroup)) {
            modelAndView = new ModelAndView("groupEdit"); // go to the page with input form
            modelAndView.addObject("currentGroup", currentGroup);
            modelAndView.addObject("currentGroupAvatar", getGroupAvatarString(currentGroup.getAvatar(), request.getContextPath()));
            logger.info("Account have permissions for editing the group. Forward to page with update form");
        } else {
            logger.info("Account doesn't have permissions for editing the group. Redirect to the group page");
            modelAndView = new ModelAndView("redirect:/group" + currentGroup.getId());
        }
        return modelAndView;
    }

    @PostMapping(value = "/editGroupHandler", consumes = {MULTIPART_FORM_DATA_VALUE})
    public String updateGroup(@ModelAttribute Group updatedGroup,
                              @SessionAttribute("currentGroup") Group currentGroup,
                              @SessionAttribute("authorizedAccount") Account authorizedAccount) {
        logger.info("Start to update group from received updated group model");
        if (logger.isDebugEnabled()) {
            logger.debug("Received a group model from update page: {}", updatedGroup);
        }
        if (hasModeratorRights(authorizedAccount, currentGroup)) {
            logger.debug("Account have permissions for editing the group");
            currentGroup.setName(updatedGroup.getName());
            currentGroup.setDescription(updatedGroup.getDescription());
            byte[] newAvatar = updatedGroup.getAvatar();
            if (newAvatar != null) {
                currentGroup.setAvatar(newAvatar);
            }
            groupService.editGroup(currentGroup);
            logger.debug("Group successfully updated");
        } else {
            logger.debug("Account doesn't have permissions for editing the group");
        }
        logger.info("Redirect to the group page");
        return "redirect:/group" + currentGroup.getId();
    }

    @GetMapping(value = "/delete")
    public String deleteGroup(@RequestParam("group") long groupId,
                              @SessionAttribute("authorizedAccount") Account authorizedAccount,
                              HttpServletRequest request) {
        logger.info("Received a request to delete group");
        Group group = groupService.get(groupId);
        if (hasCreatorRights(authorizedAccount, group)) {
            logger.debug("Account have permissions for delete the group");
            groupService.deleteGroup(group.getId());
            logger.info("Group successfully deleted");
        } else {
            logger.debug("Account doesn't have permissions for delete the group. Group not deleted");
        }
        logger.info("Redirect to the previous page");
        return "redirect:/groups?id=" + authorizedAccount.getId();
    }

    // Membership management logic

    @GetMapping(value = "/acceptSubscription")
    public String acceptRequest(@RequestParam("id") long requesterId,
                                @SessionAttribute("currentGroup") Group currentGroup,
                                @SessionAttribute("authorizedAccount") Account authorizedAccount,
                                HttpServletRequest request) {
        logger.info("Received a request to accept subscription from account");
        if (hasModeratorRights(authorizedAccount, currentGroup)) {
            logger.debug("Account have permissions to accept requests to the group");
            groupService.acceptRequest(currentGroup, requesterId);
            logger.debug("Request successfully accepted");
        }
        logger.info("Redirect to previous page");
        return "redirect:" + request.getHeader("Referer");
    }

    @GetMapping(value = "/declineSubscription")
    public String declineRequest(@RequestParam("id") long requesterId,
                                 @SessionAttribute("currentGroup") Group currentGroup,
                                 @SessionAttribute("authorizedAccount") Account authorizedAccount,
                                 HttpServletRequest request) {
        logger.info("Received a request to decline subscription from account");
        if (hasModeratorRights(authorizedAccount, currentGroup)) {
            logger.debug("Account have permissions to decline requests to the group");
            groupService.declineRequest(currentGroup, requesterId);
            logger.debug("Request successfully declined");
        }
        logger.info("Redirect to previous page");
        return "redirect:" + request.getHeader("Referer");
    }


    @GetMapping(value = "/removeSubscriber")
    public String removeGroupMember(@RequestParam("id") long memberId,
                                    @SessionAttribute("currentGroup") Group currentGroup,
                                    @SessionAttribute("authorizedAccount") Account authorizedAccount,
                                    HttpServletRequest request) {
        logger.info("Received a request to remove a member from group");
        if (hasModeratorRights(authorizedAccount, currentGroup)) {
            logger.debug("Account have permissions to delete the subscriber");
            groupService.removeGroupMember(currentGroup, memberId);
            logger.debug("Group member successfully removed");
        }
        logger.info("Redirect to previous page");
        return "redirect:" + request.getHeader("Referer");
    }

    @GetMapping(value = "/dismissModerator")
    public String dismissFromModerator(@RequestParam("id") long moderatorId,
                                       @SessionAttribute("currentGroup") Group currentGroup,
                                       @SessionAttribute("authorizedAccount") Account authorizedAccount,
                                       HttpServletRequest request) {
        logger.info("Received a request to dismiss the moderator to user");
        if (hasModeratorRights(authorizedAccount, currentGroup)) {
            logger.debug("Account have permissions to dismiss the moderator");
            groupService.dismissFromModerator(currentGroup, moderatorId);
            logger.debug("Moderator successfully dismissed to user");
        }
        logger.info("Redirect to previous page");
        return "redirect:" + request.getHeader("Referer");
    }

    @GetMapping(value = "/promoteSubscriber")
    public String promoteToModerator(@RequestParam("id") long subscriberId,
                                     @SessionAttribute("currentGroup") Group currentGroup,
                                     @SessionAttribute("authorizedAccount") Account authorizedAccount,
                                     HttpServletRequest request) {
        logger.info("Received a request to promote subscriber to moderator");
        if (hasModeratorRights(authorizedAccount, currentGroup)) {
            logger.debug("Account have permissions to promote the subscriber to moderator");
            groupService.promoteToModerator(currentGroup, subscriberId);
            logger.debug("Subscriber successfully promoted to moderator");
        }
        logger.info("Redirect to previous page");
        return "redirect:" + request.getHeader("Referer");
    }

    // Utils

    private boolean hasCreatorRights(Account user, Group group) {
        return user != null && user.equals(group.getCreator());
    }

    private boolean hasModeratorRights(Account user, Group group) {
        Set<Account> moderators = group.getModerators();
        return user != null && (hasCreatorRights(user, group) || moderators.contains(user));
    }
}