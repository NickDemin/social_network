package com.getjavajob.training.web1701.dyominn.webapp.controllers;

import com.getjavajob.training.web1701.dyominn.common.models.Account;
import com.getjavajob.training.web1701.dyominn.service.AccountService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.concurrent.TimeUnit;

import static com.getjavajob.training.web1701.dyominn.webapp.utils.ImageBytesEncoder.getAccountAvatarString;
import static java.lang.Boolean.TRUE;

@Controller
public class AuthenticationController {
    private static final Logger logger = LoggerFactory.getLogger(AuthenticationController.class);
    private AccountService accountService;

    @Autowired
    public AuthenticationController(AccountService accountService) {
        this.accountService = accountService;
    }

    @PostMapping("/accountValidationHandler")
    public String login(@RequestParam("email") String email,
                        @RequestParam("password") String password,
                        @RequestParam(value = "rememberMe", required = false) String rememberMeFlag,
                        HttpServletRequest request,
                        HttpServletResponse response,
                        HttpSession session) {
        logger.info("Start to validate authentication data");
        Account authorizedAccount = accountService.getAccountForAuthorisation(email, password, false);
        if (authorizedAccount != null) {
            session.setAttribute("authorizedAccount", authorizedAccount);
            session.setAttribute("authorizedAccountAvatar",
                    getAccountAvatarString(authorizedAccount.getAvatar(), request.getContextPath()));
            if (rememberMeFlag != null) {
                setAuthorisationCookies(response, authorizedAccount.getEmail(), authorizedAccount.getPassword());
                session.setAttribute("rememberedAccount", TRUE);
            }
            logger.info("Successfully authenticated. Redirecting to account page");
            return "redirect:/id" + authorizedAccount.getId();
        } else {
            session.setAttribute("loginFailed", TRUE);
            logger.info("Authentication is failed. Redirecting to login page");
            return "redirect:/login";
        }
    }

    private void setAuthorisationCookies(HttpServletResponse resp, String email, String password) {
        Cookie emailCookie = new Cookie("email", email);
        Cookie hashCookie = new Cookie("hash", password);
        emailCookie.setMaxAge((int) TimeUnit.DAYS.toMinutes(30));
        hashCookie.setMaxAge((int) TimeUnit.DAYS.toMinutes(30));
        resp.addCookie(emailCookie);
        resp.addCookie(hashCookie);
    }

    @GetMapping("/logout")
    public String logout(HttpSession session, HttpServletResponse response) {
        logger.info("Start to logout");
        session.invalidate();
        Cookie emailCookie = new Cookie("email", "");
        Cookie hashCookie = new Cookie("hash", "");
        emailCookie.setMaxAge(0);
        hashCookie.setMaxAge(0);
        response.addCookie(emailCookie);
        response.addCookie(hashCookie);
        logger.info("Successfully logged out. Redirecting to login page");
        return "redirect:/login";
    }
}