package com.getjavajob.training.web1701.dyominn.webapp.controllers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class CommonController {
    private static final Logger logger = LoggerFactory.getLogger(CommonController.class);

    /**
     * Handler which redirects to error page in cause of thrown server exceptions. Works with all request methods
     */
    @RequestMapping("/exceptionHandler")
    public String handleServerException() {
        logger.error("Server exception was thrown. Redirect to error page");
        return "redirect:/error";
    }
}