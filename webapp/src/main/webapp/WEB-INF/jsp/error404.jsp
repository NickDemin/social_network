<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@page pageEncoding="UTF-8" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:set var="root" value="${pageContext.request.contextPath}"/>
<!DOCTYPE html>
<html>
<head>
    <title>404 - Page not found</title>
</head>
<body style="background: #D8DDE4">
<div id="wrapper" style="width:100%; text-align:center">
    <a href="${root}/login">
        <img style="width: 65%; margin: 10% 5% 0 0" src="${root}/images/404-logo.png"/>
    </a>
</div>
</body>
</html>