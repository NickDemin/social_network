<%@page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:useBean id="imageEncoder" class="com.getjavajob.training.web1701.dyominn.webapp.utils.ImageBytesEncoder"/>
<c:set var="root" value="${pageContext.request.contextPath}"/>
<c:set var="authorizedAccount" value="${sessionScope.authorizedAccount}"/>
<c:set var="isMyFriends" value="${requestScope.isMyFriends}"/>
<c:set var="friends" value="${requestScope.friends}"/>
<c:set var="incomingRequests" value="${requestScope.incomingRequests}"/>
<c:set var="outgoingRequests" value="${requestScope.outgoingRequests}"/>

<!DOCTYPE html>
<html>
<head>
    <title>Friends</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet" href="${root}/css/common.css">
    <link rel="stylesheet" href="${root}/css/header.css">
    <link rel="stylesheet" href="${root}/css/entityLists.css">
    <script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
    <%--Autocomplete's dependencies--%>
    <link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/smoothness/jquery-ui.css">
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script src="${root}/scripts/searchAutocomplete.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>
<%@ include file="header.jsp" %>

<div id="entityListMainForm">

    <ul class="col-sm-offset-2 col-sm-2 well" id="entity-list-menu">
        <ul class="nav nav-pills nav-stacked">
            <li class="active"><a data-toggle="tab" href="#allFriends">All friends</a></li>
            <c:if test="${isMyFriends}">
                <li><a data-toggle="tab" href="#incomingFriendRequests">Incoming requests</a></li>
                <li><a data-toggle="tab" href="#outgoingFriendRequests">Outgoing requests</a></li>
            </c:if>
        </ul>
    </ul>

    <div class="col-sm-5 well">
        <div class="tab-content">
            <div id="allFriends" class="tab-pane fade in active">
                <c:forEach items="${friends}" var="account">
                    <div class="col-sm-12 entity-element">
                        <div class="row col-sm-11 entity-subelement">
                            <a href="${root}/id${account.id}">
                                <c:set var="avatar"
                                       value="${imageEncoder.getAccountAvatarString(account.avatar, root)}"/>
                                <img class="entity-avatar" src="${avatar}" alt="Avatar">
                                    ${account.firstName} ${account.lastName}
                            </a>
                        </div>
                        <c:if test="${isMyFriends}">
                            <a href="${root}/remove?friend=${account.id}"
                               class="btn btn-sm btn-danger pull-right entity-element-single-button" type="button">Delete
                            </a>
                        </c:if>
                    </div>
                </c:forEach>
                <div class="col-sm-12 divider-line"></div>
            </div>
            <div id="incomingFriendRequests" class="tab-pane fade">
                <c:forEach items="${incomingRequests}" var="account">
                    <div class="col-sm-12 entity-element">
                        <div class="row col-sm-8 entity-subelement">
                            <a href="${root}/id${account.id}">
                                <c:set var="avatar"
                                       value="${imageEncoder.getAccountAvatarString(account.avatar, root)}"/>
                                <img class="entity-avatar" src="${avatar}" alt="Avatar">
                                    ${account.firstName} ${account.lastName}
                            </a>
                        </div>
                        <div class="btn-group pull-right col-sm-4 acceptAndDeclineButton">
                            <a href="${root}/acceptFriendship?id=${account.id}"
                               class="btn btn-sm btn-success sub-button" type="submit">Accept
                            </a>
                            <a href="${root}/declineFriendship?id=${account.id}"
                               class="btn btn-sm btn-danger sub-button" type="submit">Decline
                            </a>
                        </div>
                    </div>
                </c:forEach>
                <div class="col-sm-12 divider-line"></div>
            </div>
            <div id="outgoingFriendRequests" class="tab-pane fade">
                <c:forEach items="${outgoingRequests}" var="account">
                    <div class="col-sm-12 entity-element">
                        <div class="row col-sm-11 entity-subelement">
                            <a href="${root}/id${account.id}">
                                <c:set var="avatar"
                                       value="${imageEncoder.getAccountAvatarString(account.avatar, root)}"/>
                                <img class="entity-avatar" src="${avatar}" alt="Avatar">
                                    ${account.firstName} ${account.lastName}
                            </a>
                        </div>
                        <a href="${root}/cancelOutgoingFriendship?id=${account.id}"
                           class="btn btn-sm btn-danger pull-right entity-element-single-button" type="submit">Cancel
                        </a>
                    </div>
                </c:forEach>
                <div class="col-sm-12 divider-line"></div>
            </div>
        </div>
    </div>
</div>
</body>
</html>