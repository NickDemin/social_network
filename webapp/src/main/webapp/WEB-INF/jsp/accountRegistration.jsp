<%@page contentType="text/html;charset=UTF-8" language="java" %>
<%@page pageEncoding="UTF-8" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:set var="root" value="${pageContext.request.contextPath}"/>

<!DOCTYPE html>
<html>
<head>
    <title>Registration</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet" href="${root}/css/common.css">
    <link rel="stylesheet" href="${root}/css/header.css">
    <link rel="stylesheet" href="${root}/css/entityData.css">
    <link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script src="${root}/scripts/jquery.maskedinput.js"></script>
    <script src="${root}/scripts/jquery.maskedinput.min.js"></script>
    <script src="${root}/scripts/accountData.js"></script>
</head>
<body>
<%@ include file="header.jsp" %>

<div class="container text-center" id="accountPageContainer">
    <div class="row">

        <div class="col-sm-offset-3 col-sm-6 well" id="accountDataBlock">
            <div class="container-fluid">
                <h4>Registration:</h4>

                <form action="${root}/registrationHandler" method="post" target="_self" enctype="multipart/form-data">
                    <div class="container-fluid well" id="avatarBlock">
                        <div class="form">
                            <output id="avatarPreview"></output>
                            <label for="avatar" class="btn btn-success btn-sm" style="margin-top: 1em;">
                                Choose photo
                            </label>
                            <input class="center-block" type="file" name="avatar" id="avatar"
                                   style="visibility:hidden;">
                        </div>
                    </div>

                    <div class="container-fluid well" id="personBlock">
                        <div class="form-horizontal">
                            <div class="form-group">
                                <label class="control-label col-sm-3 dataHeader" for="fname">First name:</label>
                                <div class="col-sm-8">
                                    <input class="form-control input-sm" type="text" name="firstName" id="fname"
                                           required>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-3 dataHeader" for="lname">Last name:</label>
                                <div class="col-sm-8">
                                    <input class="form-control input-sm" type="text" name="lastName" id="lname"
                                           required>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-3 dataHeader" for="mname">Middle name:</label>
                                <div class="col-sm-8">
                                    <input class="form-control input-sm" type="text" name="middleName" id="mname">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-3 dataHeader" for="bday">Birth date:</label>
                                <div class="col-sm-8">
                                    <input class="form-control input-sm" type="text" name="birthDate" id="bday"
                                           placeholder="yyyy-mm-dd">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-3 dataHeader" for="email">E-mail:</label>
                                <div class="col-sm-8">
                                    <input class="form-control input-sm" type="email" name="email" id="email" required>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-3 dataHeader" for="pass">Password:</label>
                                <div class="col-sm-8">
                                    <input class="form-control input-sm" type="password" name="password" id="pass"
                                           required>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="container-fluid well" id="phonesBlock">
                        <div class="form-horizontal">
                            <div class="form-group" id="personalPhonesBlock">
                                <div class="phone-group">
                                    <label class="control-label col-sm-3 dataHeader" for="personalPhoneInput">
                                        Personal phone:
                                    </label>
                                    <div class="col-sm-8">
                                        <input class="phoneNumberField form-control input-sm" type="text"
                                               id="personalPhoneInput" placeholder="+7(***)***-**-**">
                                    </div>
                                    <span class="addFieldButton btn btn-sm btn-link glyphicon glyphicon-plus"
                                          onclick="addPhoneField('personalPhonesBlock', 'personal', 'personalPhoneInput')">
                                    </span>
                                </div>
                            </div>
                            <div class="form-group" id="workPhonesBlock">
                                <div class="phone-group">
                                    <label class="control-label col-sm-3 dataHeader" for="workPhoneInput">
                                        Work phone:
                                    </label>
                                    <div class="col-sm-8">
                                        <input class="phoneNumberField form-control input-sm" type="text"
                                               id="workPhoneInput" placeholder="+7(***)***-**-**">
                                    </div>
                                    <span class="addFieldButton btn btn-sm btn-link glyphicon glyphicon-plus"
                                          onclick="addPhoneField('workPhonesBlock', 'work', 'workPhoneInput')"></span>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="container-fluid well" id="contactsBlock">
                        <div class="form-horizontal">
                            <div class="form-group">
                                <label class="control-label col-sm-3 dataHeader" for="hAdrs">Home address:</label>
                                <div class="col-sm-8">
                                    <input class="form-control input-sm" type="text" name="homeAddress" id="hAdrs">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-3 dataHeader" for="wAdrs">Work address:</label>
                                <div class="col-sm-8">
                                    <input class="form-control input-sm" type="text" name="workAddress" id="wAdrs">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-3 dataHeader" for="icq">ICQ:</label>
                                <div class="col-sm-8">
                                    <input class="form-control input-sm" type="text" name="icq" id="icq">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-3 dataHeader" for="skype">Skype:</label>
                                <div class="col-sm-8">
                                    <input class="form-control input-sm " type="text" name="skype" id="skype">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-3 dataHeader" for="addInf">Additional info:</label>
                                <div class="col-sm-8">
                                    <textarea class="form-control input-sm" name="additionalInfo"
                                              id="addInf"></textarea>
                                </div>
                            </div>
                        </div>
                    </div>

                    <p id="dialogParametrizedText" style="text-align: center;" hidden></p>
                    <button class="btn btn-success btn-sm" type="submit" id="commitBtn">Register</button>
                </form>
            </div>
        </div>
    </div>
</div>

<script>
    document.getElementById('avatarPreview').innerHTML = ['<img src="${root}/images/avatar-placeholder.png" width="200" />'].join('');
</script>
<script src="${root}/scripts/avatarPreview.js"></script>

</body>
</html>